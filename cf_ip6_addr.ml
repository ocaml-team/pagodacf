(*---------------------------------------------------------------------------*
  IMPLEMENTATION  cf_ip6_addr.ml

  Copyright (c) 2003-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

external init_: unit -> unit = "cf_ip6_addr_init";;
init_ ();;

type -'a t

type opaque = [ `AF_INET6 ]

type format = Unspecified | Unicast | Multicast

type unspecified = [ opaque | `X ]
type unicast = [ opaque | `U ]
type multicast = [ opaque | `M ]

external format: opaque t -> format = "cf_ip6_addr_format"

external is_unicast:
    [> opaque ] t -> unicast t = "cf_ip6_addr_is_unicast"
external is_multicast:
    [> opaque ] t -> multicast t = "cf_ip6_addr_is_multicast"

type unicast_format =
    | U_unassigned
    | U_reserved
    | U_loopback
    | U_v4compat
    | U_v4mapped
    | U_link
    | U_site
    | U_uniqlocal
    | U_global

external unicast_format:
    [> unicast] t -> unicast_format = "cf_ip6_addr_unicast_format"

type v4compat = [ unicast | `V4C ]
external is_v4compat:
    [> unicast ] t -> Cf_ip4_addr.opaque Cf_ip4_addr.t =
    "cf_ip6_addr_is_v4compat"
external to_v4compat:
    [> Cf_ip4_addr.unicast ] Cf_ip4_addr.t -> v4compat t =
    "cf_ip6_addr_to_v4compat"

type v4mapped = [ unicast | `V4M ]
external is_v4mapped:
    [> unicast ] t -> Cf_ip4_addr.opaque Cf_ip4_addr.t =
    "cf_ip6_addr_is_v4mapped"
external to_v4mapped:
    [> Cf_ip4_addr.unicast ] Cf_ip4_addr.t -> v4mapped t =
    "cf_ip6_addr_to_v4mapped"

type multicast_flag = M_F_transient | M_F_unassigned of int

type multicast_scope =
    | M_S_node
    | M_S_link
    | M_S_site
    | M_S_org
    | M_S_global
    | M_S_unassigned of int

type multicast_group_id = [ opaque | `G ]
external to_multicast_components:
    [> multicast ] t ->
    multicast_scope * multicast_flag list * multicast_group_id t =
    "cf_ip6_addr_to_multicast_components"
external of_multicast_components:
    multicast_scope -> multicast_flag list -> multicast_group_id t ->
    multicast t = "cf_ip6_addr_of_multicast_components"

external unspecified_: unit -> unspecified t = "cf_ip6_addr_unspecified"
external loopback_: unit -> unicast t = "cf_ip6_addr_loopback"
external node_local_all_nodes_:
    unit -> multicast t = "cf_ip6_addr_node_local_all_nodes"
external link_local_all_nodes_:
    unit -> multicast t = "cf_ip6_addr_link_local_all_nodes"
external link_local_all_routers_:
    unit -> multicast t = "cf_ip6_addr_link_local_all_routers"

let unspecified = unspecified_ ()
let loopback = loopback_ ()
let node_local_all_nodes = node_local_all_nodes_ ()
let link_local_all_nodes = link_local_all_nodes_ ()
let link_local_all_routers = link_local_all_routers_ ()

external equal: ([> opaque ] t as 'a) -> 'a -> bool = "cf_ip6_addr_equal"
external compare: ([> opaque ] t as 'a) -> 'a -> int =
    "cf_ip6_addr_compare_aux"

external pton: string -> opaque t option = "cf_inet_pton6"
external ntop: [> opaque ] t -> string = "cf_inet_ntop6"

(*--- End of File [ cf_ip6_addr.ml ] ---*)
