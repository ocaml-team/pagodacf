(*---------------------------------------------------------------------------*
  INTERFACE  cf_smonad.mli

  Copyright (c) 2002-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

(** The state monad and its operators. *)

(** {6 Overview}

    The state monad is provided here mostly for reference purposes.  It is not
    actually used directly within the [cf] library.  However, it is sometimes
    helpful to have an example of the state monad to use when lifting another
    monad into a new monad with state.
*)

(** {6 Types} *)

(** The state monad.  Evaluate by calling it with an initial state. *)
type ('s, 'a) t = 's -> 'a * 's

(** {6 Modules } *)

(** A module containing the [( >>= )] binding operator for composition of state
    monads.
*)
module Op: sig
    (** Use [m >>= f] to produce a monad that applies [f] to the result of
        evaluating [m].
    *)
    val ( >>= ): ('s, 'a) t -> ('a -> ('s, 'b) t) -> ('s, 'b) t
end

(** {6 Operators} *)

(** A monad that returns [unit] and performs no operation. *)
val nil: ('s, unit) t

(** Use [return a] to produce a monad that returns [a] when evaluated. *)
val return: 'a -> ('s, 'a) t

(** A monad that returns the encapsulate state. *)
val load: ('s, 's) t

(** Use [store s] to produce a monad with [s] as the value of its encapsulated
    state.
*)
val store: 's -> ('s, unit) t

(** Use [modify f] to produce a monad that applies [f] to the encapsulated
    state to obtain a new state value, and which returns the unit value as its
    result when evaluated.
*)
val modify: ('s -> 's) -> ('s, unit) t

(** Use [field f] to produce a monad that returns the result of applying [f] to
    the value of the encapsulated state.
*)
val field: ('s -> 'a) -> ('s, 'a) t      (* let field f s = (f s), s         *)

(*--- End of File [ cf_smonad.mli ] ---*)
