(*---------------------------------------------------------------------------*
  INTERFACE  cf_flow.mli

  Copyright (c) 2002-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

(** Lazy stream procesors and their operators. *)

(** {6 Overview}
    A [Cf_flow] value is like a [Cf_seq] value that can take intermediate input
    to continue generating output.  Many of the other modules in the [cf]
    library use this module.
    
    The semantics of this module are derived from the stream processors in the
    Fudgets system, as described by Magnus Carlsson and Thomas Hallgren in
    their joint {{:http://www.cs.chalmers.se/~hallgren/Thesis/}Ph.D. thesis},
    chapter 16.
*)

(** {6 Types} *)

(** A stream processor *)
type ('i, 'o) t = ('i, 'o) cell Lazy.t
and ('i, 'o) cell =
    | P of 'o * ('i,'o) t           (** Output a value *)
    | Q of ('i -> ('i, 'o) cell)    (** Input a value *)
    | Z                             (** Finish processing stream *)

(** {6 Constructors} *)

(** A stream processor that reads no input and writes no output. *)
val nil: ('y, 'x) t

(** A stream processor that outputs every input value without change. *)
val nop: ('x, 'x) t

(** Use [filter f] to construct a stream processor that applies [f] to every
    input value and outputs only those for which the function result is [true].
*)
val filter: ('x -> bool) -> ('x, 'x) t

(** Use [map f] to construct a stream processor that applies [f] to every input
    value and outputs the result.
*)
val map: ('i -> 'o) -> ('i, 'o) t

(** Use [optmap f] to construct a stream processor that applies [f] to every
    input value and outputs the result if there is one.
*)
val optmap: ('i -> 'o option) -> ('i, 'o) t

(** Use [listmap f] to construct a stream processor that applies [f] to every
    input value and outputs every element of the resulting list.
*)
val listmap: ('i -> 'o list) -> ('i, 'o) t

(** Use [listmap f] to construct a stream processor that applies [f] to every
    input value and outputs every element of the resulting sequence.
*)
val seqmap: ('i -> 'o Cf_seq.t) -> ('i, 'o) t

(** Use [broadcast ws] to construct a stream processor that combines the input
    and output of every stream processor in the list [ws] by first rendering
    all the output from each stream in turn, then ingesting all the input to
    each stream in turn, until all streams are completed.
*)
val broadcast: ('i, 'o) t list -> ('i, 'o) t

(** Use [mapstate f s] with an initial state value [s] and a folding function
    [f] to construct a stream processor that folds the state into every input
    value to produce an output value and a new state.
*)
val mapstate: ('s -> 'i -> 's * 'o) -> 's -> ('i, 'o) t

(** Use [machine f s] with an initial state value [s] and a folding function
    [f] to construct a stream processor that folds the state into every input
    value to produce either a sequence of values to output and a new state or
    the end of stream processing.
*)
val machine: ('s -> 'i -> ('s * 'o Cf_seq.t) option) -> 's -> ('i, 'o) t

(** {6 Operators} *)

(** Open this module to bring the operator functions into the current scope. *)
module Op: sig
    (** Broadcasting parallel composition. *)
    val ( -*- ): ('i, 'o) t -> ('i, 'o) t -> ('i, 'o) t
    
    (** Serial composition. *)
    val ( -=- ): ('i, 'x) t -> ('x, 'o) t -> ('i, 'o) t
    
    (** Sequential composition. *)
    val ( -&- ): ('i, 'o) t -> ('i, 'o) t -> ('i, 'o) t
    
    (** Tagged parallel composition. *)
    val ( -+- ):
      ('ia, 'oa) t -> ('ib, 'ob) t ->
      (('ia, 'ib) Cf_either.t, ('oa, 'ob) Cf_either.t) t
    
    (** Serial loop composition. *)
    val ( ~@ ): ('x, 'x) t -> ('x, 'x) t
    
    (** Serial loop left. *)
    val ( ~@< ): (('x, 'i) Cf_either.t, ('x, 'o) Cf_either.t) t -> ('i, 'o) t

    (** Serial loop through right. *)
    val ( -@- ):
        (('o0, 'i1) Cf_either.t, ('i0, 'o1) Cf_either.t) t -> ('i0, 'o0) t ->
        ('i1, 'o1) t
end

(** {6 Miscellaneous} *)

(** Use [to_seq w] to convert a stream processor [w] into the equivalent
    sequence.  This can only work when the stream processor ingests input of
    the [unit] type.
*)
val to_seq: (unit, 'o) t -> 'o Cf_seq.t

(** Use [of_seq z] to convert a sequence into the equivalent stream processor
    (which never ingests any input).
*)
val of_seq: 'o Cf_seq.t -> ('i, 'o) t

(** A stream processor that converts uppercase US-ASCII characters into
    lowercase characters.  All other characters are unchanged.
*)
val upcase: (char, char) t

(** A stream processor that converts lowercase US-ASCII characters into
    uppercase characters.  All other characters are unchanged.
*)
val dncase: (char, char) t

(** Use [commute w z] to produce an output sequence from a flow [w] that
    ingests its input from the sequence [z].
*)
val commute: ('i, 'o) t -> 'i Cf_seq.t -> 'o Cf_seq.t

(** Use [commute_string w s] to commute the sequence of characters in the string
    [s] with the flow [w] and compose a new string from the resulting sequence.
*)
val commute_string: (char, char) t -> string -> string

(** Use [drain w] to produce an output sequence comprised of all the values
    output from the stream processor [w] until the first input is required.
*)
val drain: ('i, 'o) t -> 'o Cf_seq.t

(** Use [flush w] to discard all the output from the flow [w] until the first
    input is required.
*)
val flush: ('i, 'o) t -> ('i, 'o) t

(** A stream processor that copies to its output every element of its input
    sequences.  The stream processor finishes when it ingests [None].
    
    This stream processor is helpful for placing at the end of a serial
    composition to produce a transcoder.
*)
val ingestor: ('a Cf_seq.t option, 'a) t

(** Use [transcode w z] to produce the sequence of output values obtained by
    executing the transcoder stream processor [w] to ingest every element of
    the sequence [z].
*)
val transcode: ('i Cf_seq.t option, 'o) t -> 'i Cf_seq.t -> 'o Cf_seq.t

(** A namespace for the [more] and [last] transcoder functions. *)
module Transcode: sig
    (** Use [more w z] to produce an intermediate sequence of output values
        obtained by executing the transcoder stream processor [w] to ingest all
        the elements of the sequence [z].  Returns the intermediate output
        sequence and a new transcoder stream processor representing the
        intermediate state of the transcoder, as it is now ready for ingesting
        more input or its "end of input" indication.
    *)
    val more:
        ('i Cf_seq.t option, 'o) t -> 'i Cf_seq.t ->
        'o Cf_seq.t * ('i Cf_seq.t option, 'o) t

    (** Use [last w z] to produce the final sequence of output values obtained
        by executing the transcoder stream processor [w] after ingesting the
        "end of input" indication.
    *)
    val last: ('i Cf_seq.t option, 'o) t -> 'o Cf_seq.t
end

(** {6 Monad Functions} *)

(** The continuation monad that returns a value obtained from the flow produced
    by its evaluation.
*)
val readC: (('i, 'o) t, 'i) Cf_cmonad.t

(** Use [writeC x] to compose a continuation monad that puts [x] into the
    flow produced by evaluation and returns the unit value.
*)
val writeC: 'o -> (('i, 'o) t, unit) Cf_cmonad.t

(** Use [evalC m] to evaluate the continuation monad [m], computing the
    encapsulated flow.
*)
val evalC: (('i, 'o) t, unit) Cf_cmonad.t -> ('i, 'o) t

(** The state-continuation monad that returns a value obtained from the flow
    produced by its evaluation.
*)
val readSC: ('s, ('i, 'o) t, 'i) Cf_scmonad.t

(** Use [writeSC x] to compose a state-continuation monad that puts [x] into
    the flow produced by evaluation and returns the unit value.
*)
val writeSC: 'o -> ('s, ('i, 'o) t, unit) Cf_scmonad.t

(** Use [evalSC m s] to evaluate the state-continuation monad [m] with the
    initial state [s], computing the encapsulated flow.
*)
val evalSC: ('s, ('i, 'o) t, unit) Cf_scmonad.t -> 's -> ('i, 'o) t

(*--- End of File [ cf_flow.mli ] ---*)
