(*---------------------------------------------------------------------------*
  INTERFACE  cf_map.mli

  Copyright (c) 2004-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

(** A module type for associative array implementations (with functional
    enhancements over the {!Map} module in the standard library).
*)

(** {6 Overview}
    
    This module defines the common interface to the various implementations of
    functional associative arrays in the {!Cf} library.
*)

module type T = sig    
    (** The tree type. *)
    type +'a t
    
    (** A module defining the type of the key.  Some map implementations may
        define more functions in this module for disambiguating keys from one
        another.
    *)
    module Key: sig type t end
            
    (** The empty tree. *)
    val nil: 'a t
    
    (** Use [empty m] to test whether the tree [m] is empty. *)
    val empty: 'a t -> bool
    
    (** Use [size m] to count the number of elements in the tree [m]. *)
    val size: 'a t -> int
    
    (** Use [min m] to obtain the key-value pair with the ordinally minimum key
        in the tree [m].  Raises [Not_found] if the tree is empty.
    *)
    val min: 'a t -> (Key.t * 'a)
    
    (** Use [max m] to obtain the key-value pair with the ordinally maximum key
        in the tree [m].  Raises [Not_found] if the tree is empty.
    *)
    val max: 'a t -> (Key.t * 'a)
    
    (** Use [search k m] to obtain the value associated with the key [k] in the
        tree [m].  Raise [Not_found] if the tree does not contain the key.
    *)
    val search: Key.t -> 'a t -> 'a
    
    (** Use [member k m] to test whether the tree [m] contains the key [k]. *)
    val member: Key.t -> 'a t -> bool

    (** Use [insert p m] to insert the key-value pair [p] into the tree [m],
        producing a new tree with the inserted element and, if the key [k] is
        already present in [m], the value replaced by the insertion.
    *)
    val insert: (Key.t * 'a) -> 'a t -> 'a t * 'a option
    
    (** Use [replace p m] to obtain a new tree produced by inserting the
        key-value pair [p] into the tree [m], replacing any existing pair
        associated to the same key.
    *)
    val replace: (Key.t * 'a) -> 'a t -> 'a t
    
    (** Use [modify k f m] to obtain a new tree produced by replacing the value
        in the tree [m] associated with the key [k] with the result of applying
        it to the continuation function [f].  Raises [Not_found] if the tree
        does not contain the key.
    *)
    val modify: Key.t -> ('a -> 'a) -> 'a t -> 'a t
    
    (** Use [extract k m] to obtain the value associated with the key [k] in
        the tree [m] and a new tree with the key deleted from the tree.  Raises
        [Not_found] if the tree does not contain the key.
    *)
    val extract: Key.t -> 'a t -> 'a * 'a t
    
    (** Use [delete k m] to obtain a new tree produced by deleting the key [k]
        from the tree [m].  If the tree does not contain the key, then the
        function simply returns its argument.
    *)
    val delete: Key.t -> 'a t -> 'a t
    
    (** Use [of_list s] to compose a tree by iterating the list of key-value
        pairs [s] and inserting them in order into a new tree.
    *)
    val of_list: (Key.t * 'a) list -> 'a t
    
    (** Use [of_list_incr s] to compose a tree with the key-value pairs in the
        ordered list [s].  Runs in linear time if the keys in the list [s] are
        known to be in increasing order.  Otherwise, there is an additional
        linear cost beyond [of_list s].
    *)
    val of_list_incr: (Key.t * 'a) list -> 'a t
    
    (** Use [of_list_decr s] to compose a tree with the key-value pairs in the
        ordered list [s].  Runs in linear time if the keys in the list [s] are
        known to be in decreasing order.  Otherwise, there is an additional
        linear cost beyond [of_list s].
    *)
    val of_list_decr: (Key.t * 'a) list -> 'a t
    
    (** Use [of_seq z] to compose a tree by evaluating the entire sequence of
        key-value pairs [z] and inserting them in order into a new tree.
    *)
    val of_seq: (Key.t * 'a) Cf_seq.t -> 'a t
    
    (** Use [of_seq_incr z] to compose a tree with the key-value pairs in the
        ordered sequence [z].  Runs in linear time if the keys in the sequence
        [z] are known to be in increasing order.  Otherwise, there is an
        additional linear cost beyond [of_seq z].
    *)
    val of_seq_incr: (Key.t * 'a) Cf_seq.t -> 'a t
    
    (** Use [of_seq_decr z] to compose a tree with the key-value pairs in the
        ordered sequence [z].  Runs in linear time if the keys in the sequence
        [z] are known to be in decreasing order.  Otherwise, there is an
        additional linear cost beyond [of_seq z].
    *)
    val of_seq_decr: (Key.t * 'a) Cf_seq.t -> 'a t

    (** Use [to_list_incr m] to obtain a sequence of the key-value pairs in the
        tree [m] in order of increasing ordinality.
    *)
    val to_list_incr: 'a t -> (Key.t * 'a) list

    (** Use [to_list_decr m] to obtain a sequence of the key-value pairs in the
        tree [m] in order of descreasing ordinality.
    *)
    val to_list_decr: 'a t -> (Key.t * 'a) list

    (** Use [to_seq_incr m] to obtain a sequence of the key-value pairs in the
        tree [m] in order of increasing ordinality.
    *)
    val to_seq_incr: 'a t -> (Key.t * 'a) Cf_seq.t

    (** Use [to_seq_decr m] to obtain a sequence of the key-value pairs in the
        tree [m] in order of descreasing ordinality.
    *)
    val to_seq_decr: 'a t -> (Key.t * 'a) Cf_seq.t

    (** Use [nearest_decr k m] to obtain the key-value pair ordinally less than
        or equal to the key [k] in the map [m].  Raises [Not_found] if the map
        is empty or all the keys are ordinally greater.
    *)
    val nearest_decr: Key.t -> 'a t -> (Key.t * 'a) Cf_seq.t
    
    (** Use [nearest_incr k m] to obtain the key-value pair ordinally greater
        than or equal to the key [k] in the map [m].  Raises [Not_found] if the
        map is empty or all the keys are ordinally less.
    *)
    val nearest_incr: Key.t -> 'a t -> (Key.t * 'a) Cf_seq.t
    
    (** Use [iterate f m] to apply the function [f] to each key-value pair in
        the tree [m] in an arbitrary order (not increasing or decreasing).
    *)
    val iterate: ((Key.t * 'a) -> unit) -> 'a t -> unit
    
    (** Use [predicate f m] to test whether all the key-value pairs in the tree
        [m] satisfy the predicate function [f].  The nodes of the tree are
        visited in an arbitrary order (not increasing or decreasing).
    *)
    val predicate: ((Key.t * 'a) -> bool) -> 'a t -> bool

    (** Use [fold f s m] to fold the every key-value pair in the tree [m] into
        the state [s] with the folding function [f], visiting the elements in
        an arbitrary order (not increasing or decreasing).  Runs in O(log N)
        space, i.e. not tail-recursive.
    *)
    val fold: ('b -> (Key.t * 'a) -> 'b) -> 'b -> 'a t -> 'b
    
    (** Use [filter f m] to obtain a new tree comprised of all the key-value
        pairs in the tree [m] that satisfy the filtering function [f].  The
        elements in [m] are visited in arbitrary order (not increasing or
        decreasing).  Runs in O(log N) space, i.e. not tail-recursive.
    *)
    val filter: ((Key.t * 'a) -> bool) -> 'a t -> 'a t
    
    (** Use [map f m] to obtain a new tree produced by applying the mapping
        function [f] to the key and the value of each key-value pair in the
        tree [m] and associating the resulting value to the key in the new
        tree.  Elements in the tree are visited in arbitrary order (not
        increasing or descreasing.  Runs in O(log N) space, i.e. not
        tail-recursive.
    *)
    val map: ((Key.t * 'a) -> 'b) -> 'a t -> 'b t
    
    (** Use [optmap f m] to obtain a new tree produced by applying the mapping
        function [f] to the key and the value of each key-value pair in the
        tree [m] and associating the resulting value to the key in the new
        tree.  If the function [f] returns [None] then no value for that key
        will be present in the new tree.  Elements in the tree are visited in
        arbitrary order (not increasing or descreasing.  Runs in O(log N)
        space, i.e. not tail-recursive.
    *)
    val optmap: ((Key.t * 'a) -> 'b option) -> 'a t -> 'b t
    
    (** Use [partition f m] to obtain a pair of new trees produced by applying
        the partitioning function [f] to all the elements in the tree [m] in an
        arbitrary order (not increasing or descreasing).  The first tree will
        contain all the elements for which [f] returns [true], and the second
        tree will have all the elements for which [f] returns [false].  Runs in
        O(log N) space, i.e. not tail-recursive.
    *)
    val partition: ((Key.t * 'a) -> bool) -> 'a t -> 'a t * 'a t
end

(*--- End of File [ cf_map.mli ] ---*)
