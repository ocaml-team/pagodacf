Open issues in development:

+ (unspecified): A GODI package would be nice to promote community adoption.

+ (Cf_gadget): Write some documentation about polymorphic variants as channel
    type parameters.

+ (Cf_poll): Alan Post writes:

  "When converting [Cf_poll] to use kqueue, I noticed that even after
  this conversion, the operations don't always scale that well:

               load      unload     service
    fd         lg n      lg n       lg n
    timer      C         n          C
    idle       C         n          C

  In each case, the "n" is the number of registered callbacks of the
  particular type.

  The signal callbacks are limited to one per signal slot, so scaling
  isn't really an applicable issue at the moment.  I didn't address
  that limitation in [Cf_poll] as a part of the kqueue patch because the
  issues were unrelated.

  In the same vein, cf_poll currently allows only one callback per fd;
  this seems sensible to me, but should probably be documented."

+ (Cf_rbtree): Document the complexity of the [incr_next] and [decr_next]
    functions.  The binary set functions could be improved for performance by
    using recursive [join] and [split] functions.

+ (Cf_socket_p): No support for WIN32.  SVR4 remains untested, but portability
    has been substantially improved.  Suse Linux 9.0+ may still work.  I don't
    know anymore, since my Linux box died, I replaced it with Mac OS X
    Server 10.4, and I haven't looked back.

+ (Cf_socket and cognates): Support for sendmsg() and recvmsg(), and associated
    IPv4 and IPv6 protocol options that use the control plane for various
    advanced features of the network protocol stack.

+ (Cf_socket and cognates): Support for AF_LOCAL.  To do this right, we need
    support for the control plane using sendmsg() and recvmsg(), so we can have
    file descriptor and user credential passing-- pretty much the only good
    reasons to use AF_LOCAL other than to communicate with other programs that
    use them instead of AF_INET on the loopback interface for no good reason.

+ (Cf_tai64_p, Cf_tai64n_p): No support for 32-bit environments where the C
    compiler does not offer support for 64-bit integers with
    'unsigned long long'.  Do such environments exist anymore?

+ (Cf_tai64,Cf_tai64n,Cf_stdtime): The leap seconds table is currently
    hard-coded into the .ml file here.  What we really need is an active daemon
    process that keeps a regular check on the IERS bulletins that announce leap
    seconds.  We also need the conversions between UTC and TAI to 1) raise an
    exception if they correspond to an epoch in the future beyond the
    expiration of the current IERS bulletin; and 2) optionally perform a
    calculation based on estimated drift between TAI and UTC.

+ (Cf_patricia): Is a generalized PATRICIA trie implementation a good idea?  If
    so, is it a good idea to implement sets on both red-black binary trees and
    on PATRICIA tries and see which one performs better.  Further research is
    in order here.
    
+ (Cf_int_patricia): An implementation of PATRICIA tries specialized for
    integer keys is worth doing separately.  Integer sets implemented on
    PATRICIA tries are probably the best way to get faster integer sets for the
    [Cf_dfa] module.

+ (Cf_uri): Need function for converting an absolute path to a relative path
    for a given base path.

+ (Unimplemented): Support for I18N and locales.  Need timezones especially.
    The rest we can probably defer to the nice people working on Camomile.

+ (Cf_nameinfo): There should be a function for acquiring an 'en-US' localized
    string corresponding to a value of type [unresolved].

+ (Cf_gadget): The scheduler might benefit from optimization for scalability.
    After guard is evaluated once and found to be pending, it should be
    converted into a map of wire numbers to gate functions.  Furthermore, the
    queue of pending guards should be compressed so that each entry is a map of
    wire numbers to a record comprised of the pending gate and the guard that
    contains it.  If no wire is gated simultaneously in more than one guard,
    then the guard queue will never have more than one entry in it, and the
    scheduler will offer O(log N) complexity, where N is the number of
    simultaneously pending wires.  Note: expiring wires will be an issue.

# End of open issues
