(*---------------------------------------------------------------------------*
  INTERFACE  cf_cmonad.mli

  Copyright (c) 2002-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

(** The continuation monad and its operators. *)

(** {6 Overview} *)

(** The continuation monad is provided here mostly for reference purposes,
    since it is helpful to refer to it when lifting into a more complex
    monad.
    
    A continuation monad represents a computation composed of stages that can
    be interrupted, resumed and rescheduled.  Because Objective Caml is an
    eager language programming in the continuation-passing style (CPS) can be
    simplified by the use of the continuation monad and its operators.
    
    Note: see the {!Cf_gadget} module for an example of its use.
*)

(** {6 Types} *)

(** The continuation monad: a function for passing intermediate results from
    continuation context to continuation context.
*)
type ('x, 'a) t = ('a -> 'x) -> 'x

(** {6 Modules} *)

(** A module containing the [( >>= )] binding operator for composition of
    continuation monads.
*)
module Op: sig
    (** Use [m >>= f] to produce a monad that applies [f] to the result of
        evaluating [m].
    *)
    val ( >>= ): ('x, 'a) t -> ('a -> ('x, 'b) t) -> ('x, 'b) t
end

(** {6 Operators} *)

(** A monad that returns [unit] and performs no operation. *)
val nil: ('x, unit) t

(** Use [return a] to produce a monad that returns [a] as an intermediate
    result from the current continuation.
*)
val return: 'a -> ('x, 'a) t

(** Use [init x] to produce a monad that discards the current intermediate
    result and returns [x] as the continuation context.
*)
val init: 'x -> ('x, 'a) t

(** Use [cont f] to produce a monad that passes the calling continuation to
    the function [f] and returns the unit value as an intermediate result.
*)
val cont: ('x -> 'x) -> ('x, unit) t

(** Use [eval m] to evaluate the continuation monad to produce a function from
    initial continuation context to final continuation context.
*)
val eval: ('x, unit) t -> 'x -> 'x

(*--- End of File [ cf_cmonad.mli ] ---*)
