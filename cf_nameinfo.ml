(*---------------------------------------------------------------------------*
  IMPLEMENTATION  cf_nameinfo.ml

  Copyright (c) 2003-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

type unresolved =
    EAI_ADDRFAMILY | EAI_AGAIN | EAI_BADFLAGS | EAI_FAIL | EAI_FAMILY |
    EAI_MEMORY | EAI_NODATA | EAI_NONAME | EAI_SERVICE | EAI_SOCKTYPE |
    EAI_BADHINTS | EAI_PROTOCOL | EAI_UNKNOWN of int

exception Unresolved of unresolved

let _ = Callback.register_exception
    "Cf_nameinfo.Unresolved" (Unresolved (EAI_UNKNOWN (-1)))

external error_message: unresolved -> string = "cf_nameinfo_error_message"

;;
external init_: unit -> unit = "cf_nameinfo_init";;
init_ ();;
    
external domain_: unit -> unit Cf_socket.domain = "cf_nameinfo_domain"
external socktype_: unit -> unit Cf_socket.socktype = "cf_nameinfo_socktype"
external protocol_: unit -> Cf_socket.protocol = "cf_nameinfo_protocol"
external unspecified_:
    unit -> unit Cf_socket.sockaddr = "cf_nameinfo_unspecified"

module P = struct
    module ST = struct
        type tag = unit
        
        let socktype = socktype_ ()
    end
    
    module AF = struct
        type tag = unit
        type address = unit Cf_socket.sockaddr

        let domain = domain_ ()
    
        let to_sockaddr = Obj.magic
        let of_sockaddr = Obj.magic
    
        let unspecified = unspecified_ ()
    end
    
    let protocol = protocol_ ()
end

external is_specific_socktype:
    unit Cf_socket.socktype -> 'st Cf_socket.socktype -> bool =
    "cf_nameinfo_is_specific_socktype"

external is_specific_domain:
    unit Cf_socket.sockaddr -> 'af Cf_socket.domain -> bool =
    "cf_nameinfo_is_specific_domain"

external specialize_sockaddr:
    unit Cf_socket.sockaddr -> 'af Cf_socket.domain ->
    'af Cf_socket.sockaddr = "cf_nameinfo_specialize_sockaddr"

type of_address_flags = {
    ni_nofqdn: bool;
    ni_numerichost: bool;
    ni_namereqd: bool;
    ni_numericserv: bool;
    ni_dgram: bool;
}

let of_address_default_flags = {
    ni_nofqdn = false;
    ni_numerichost = false;
    ni_namereqd = false;
    ni_numericserv = false;
    ni_dgram = false;
}

external of_address:
    ?host:int -> ?serv:int -> ?flags:of_address_flags ->
    'a Cf_socket.sockaddr -> string * string = "cf_nameinfo_of_address"

type to_address_flags = {
    ai_passive: bool;
    ai_canonname: bool;
    ai_numerichost: bool;
}

external default_ai_flags_:
    unit -> to_address_flags = "cf_nameinfo_default_ai_flags"

let to_address_default_flags = default_ai_flags_ ()

type to_address_arg =
    | A_nodename of string
    | A_servicename of string
    | A_bothnames of string * string

type ('af, 'st) addrinfo = {
    ai_flags: to_address_flags;
    ai_family: 'af Cf_socket.domain;
    ai_socktype: 'st Cf_socket.socktype;
    ai_protocol: Cf_socket.protocol;
    ai_cname: string option;
    ai_addr: 'af Cf_socket.sockaddr;
}

let nzero_ = Nativeint.of_int 0

let addrinfo_hint ?(flags = to_address_default_flags) af st p = {
    ai_flags = flags;
    ai_family = af;
    ai_socktype = st;
    ai_protocol = p;
    ai_cname = None;
    ai_addr = Obj.magic P.AF.unspecified;
}

let addrinfo_default_hint = addrinfo_hint P.AF.domain P.ST.socktype P.protocol

external to_addresses:
    ('af, 'st) addrinfo -> to_address_arg -> ('af, 'st) addrinfo list =
    "cf_nameinfo_to_addresses"

let to_all_addresses arg = to_addresses addrinfo_default_hint arg

(*--- End of File [ cf_nameinfo.ml ] ---*)
