(*---------------------------------------------------------------------------*
  INTERFACE  cf_rbtree.mli

  Copyright (c) 2004-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

(** Functional red-black binary trees. *)

(** {6 Overview}
    
    This module implements functional sets and maps based on red-black binary
    trees.  This permits trees that can be used as an alternative to the [Set]
    and [Map] modules in the Ocaml standard library.  For many operations on
    sets and maps, red-black binary trees give better performance that the
    balanced trees in the standard library (though some applications may see
    better performance with the standard modules).
*)

(** {6 Module} *)

(** A functor that produces a module of type [Cf_set] to represent sets with
    the element type described by [E].
*)
module Set(E: Cf_ordered.Total_T): Cf_set.T with module Element = E

(** A functor that produces a module of type [Cf_map] to represent maps with
    keys of the type described by [K].
*)
module Map(K: Cf_ordered.Total_T): Cf_map.T with module Key = K

(*--- End of File [ cf_rbtree.mli ] ---*)
