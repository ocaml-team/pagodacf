(*---------------------------------------------------------------------------*
  IMPLEMENTATION  cf_lex.ml

  Copyright (c) 2005-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

module DFA = Cf_regex.DFA

type x = DFA.x
type 'a r = 'a DFA.r
type 'a t = 'a DFA.t

let nil = DFA.nil
let create r = DFA.create r

module type Expr_Op_T = sig
    val ( $| ): x -> x -> x
    val ( $& ): x -> x -> x

    val ( !* ): x -> x
    val ( !+ ): x -> x
    val ( !? ): x -> x
    val ( !: ): char -> x
    val ( !^ ): (char -> bool) -> x
    val ( !~ ): char Cf_seq.t -> x
    val ( !$ ): string -> x
end

module Expr_Op = struct    
    let ( !~ ) = Cf_regex.expr_of_seq
    let ( !$ ) = Cf_regex.expr_of_string
end

let identity_ x = x

module Op = struct
    include DFA.Op
    include Expr_Op
        
    let ( $> ) e f = e $> (fun z -> f (Cf_seq.to_string z))
    let ( ?~ ) e = DFA.create (e $> identity_)
    let ( ?$ ) s = DFA.create (Cf_regex.expr_of_string s $> identity_)
end

module X = struct
    type ('c, 'a) r = ('c, 'a) DFA.X.r
    type ('c, 'a) t = ('c, 'a) DFA.X.t

    let create r = DFA.X.create r

    module Op = struct
        include DFA.X.Op
        include Expr_Op

        let ( $> ) e f = e $> (fun z -> f (Cf_seq.to_string z))
        let ( ?~ ) e = DFA.X.create (e $> identity_)
        let ( ?$ ) s = DFA.X.create (Cf_regex.expr_of_string s $> identity_)
    end
end

type counter = {
    c_pos: int;
    c_row: int;
    c_col: int;
}

let counter_zero = {
    c_pos = 0;
    c_row = 0;
    c_col = 0;
}

class cursor ?(c = counter_zero) newline =
    let nl0 = Cf_seq.to_list (Cf_seq.of_string newline) in
    object(self:'self)
        inherit [char] Cf_parser.cursor c.c_pos

        val row_: int = c.c_row
        val col_: int = c.c_col
        val nlz_: char list = nl0
        val nl0_: char list = nl0
        
        method counter = {
            c_pos = position_;
            c_row = row_;
            c_col = col_;
        }
        
        method row = row_
        method col = col_
        
        method private next ch =
            match nlz_ with
            | hd :: [] when ch = hd -> succ row_, 0, nl0_
            | hd :: tl when ch = hd -> row_, succ col_, tl
            | _ -> row_, succ col_, nlz_
        
        method advance ch =
            let row, col, nlz = self#next ch in {<
                position_ = succ position_;
                row_ = row;
                col_ = col;
                nlz_ = nlz;
            >}
    end

(*--- End of File [ cf_lex.ml ] ---*)
