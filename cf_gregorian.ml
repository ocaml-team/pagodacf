(*---------------------------------------------------------------------------*
  IMPLEMENTATION  cf_gregorian.ml

  Copyright (c) 2003-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

let of_mjd =
    let rec loop year day =
        if day >= 146097 then loop (succ year) (day - 146097) else year, day
    in
    fun ?wday ?yday day ->
        let year = day / 146097 in
        let day = (day mod 146097) + 678881 in
        let year, day = loop year day in
        begin
            match wday with
            | None -> ()
            | Some wday -> wday := (day + 3) mod 7
        end;
        let year = year * 4 in
        let year, day =
            if day = 146096 then
                year + 3, 36524
            else
                year + (day / 36524), day mod 36524
        in
        let year = ((year * 25) + (day / 1461)) * 4 in
        let day = day mod 1461 in
        let yd = if day < 306 then 1 else 0 in
        let year, day =
            if day = 1460 then
                year + 3, 365
            else
                year + (day / 365), day mod 365
        in
        let yd = yd + day in
        let day = day * 10 in
        let month =  (day + 5) / 306 in
        let day = (day + 5) mod 306 in
        let day = day / 10 in
        let yd, year, month =
            if month >= 10 then
                yd - 306, succ year, month - 10
            else
                yd + 59, year, month + 2
        in
        begin
            match yday with
            | None -> ()
            | Some yday -> yday := yd
        end;
        year, succ month, succ day

let of_cjd ?wday ?yday day =
    if day < (min_int + 2400001) then
        invalid_arg "Cf_gregorian: chronological Julian day too old.";
    of_mjd ?wday ?yday (day - 2400001)

let to_mjd =
    let times365 = [| 0; 365; 730; 1095 |] in
    let times36524 = [| 0; 36524; 73048; 109572 |] in
    let montab = [| 0; 31; 61; 92; 122; 153; 184; 214; 245; 275; 306; 337 |] in
    let adjday y d = y mod 400, d + (146097 * (y / 400)) in
    fun ~year:y ~month:m ~day:d ->
        let d = d - 678882 and m = pred m in
        let y, d = adjday y d in
        let y, m = if m >= 2 then y, m - 2 else pred y, m + 10 in
        let y = y + (m / 12) and m = m mod 12 in
        let y, m = if m < 0 then pred y, m + 12 else y, m in
        let y, d = adjday y (d + (Array.unsafe_get montab m)) in
        let y, d = if y < 0 then y + 400, d - 146097 else y, d in
        let d = d + (Array.unsafe_get times365 (y land 3)) and y = y lsr 2 in
        let d = d + 1461 * (y mod 25) and y = y / 25 in
        d + (Array.unsafe_get times36524 (y land 3))

let to_cjd_unsafe ~year ~month ~day =
    let d = to_mjd ~year ~month ~day in
    d + 2400001

let minyear_, minmonth_, minday_ = of_cjd (min_int + 2400001)
let maxyear_, maxmonth_, maxday_ = of_cjd max_int
let is_leap_year_ y = y mod 4 = 0 && y mod 100 <> 0 || y mod 400 = 0
let md_nonleap_ = [| 31; 28; 31; 30; 31; 30; 31; 31; 30; 31; 30; 31 |]
let md_leap_ = [| 31; 29; 31; 30; 31; 30; 31; 31; 30; 31; 30; 31 |]

let is_valid ~year:y ~month:m ~day:d =
    m >= 1 && m <= 12 &&
    d >= 1 && d <= 31 &&
    begin
        let md = if is_leap_year_ y then md_leap_ else md_nonleap_ in
        d <= Array.unsafe_get md (pred m) &&
        y >= minyear_ && y <= maxyear_ &&
        not (y = minyear_ &&
            (m < minmonth_ || m = minmonth_ && d < minday_)) &&
        not (y = maxyear_ &&
            (m > maxmonth_ || m = maxmonth_ && d > maxday_))
    end

let to_cjd ~year:y ~month:m ~day:d =
    if m < 1 || m > 12 then invalid_arg "Cf_gregorian.to_mjd: month 1..12";
    if d < 1 || d > 31 then invalid_arg "Cf_gregorian.to_mjd: day 1..31";
    let md = if is_leap_year_ y then md_leap_ else md_nonleap_ in
    if d > Array.unsafe_get md (pred m) then
        invalid_arg "Cf_gregorian.to_mjd: date not gregorian";
    if y < minyear_ || y > maxyear_ ||
        (y = minyear_ && (m < minmonth_ || m = minmonth_ && d < minday_)) ||
        (y = maxyear_ && (m > maxmonth_ || m = maxmonth_ && d > maxday_))
        then
            invalid_arg "Cf_gregorian.to_mjd: integer overflow";
    to_cjd_unsafe ~year:y ~month:m ~day:d

(*--- End of File [ cf_gregorian.ml ] ---*)
