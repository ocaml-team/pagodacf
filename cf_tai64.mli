(*---------------------------------------------------------------------------*
  INTERFACE  cf_tai64.mli

  Copyright (c) 2003-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

(** Computations with the Temps Atomique International (TAI) timescale. *)

(** {6 Overview}

    This module defines an abstract type and associated functions for
    computations with values representing epochs in the Temps Atomique
    International (TAI) timescale.  Values are represented internally with the
    TAI64 format defined by Dan Bernstein, and support precision to the nearest
    second.
    
    Functions are provided that:
    - acquire the current time in TAI64 format.
    - compare, add and subtract values.
    - convert between TAI64 values and a portable external format called
      the "TAI64 label", which is essentially an array of eight octets.
    - convert between TAI64 values and the float values returned by the
      [Unix.time] function.
    
    Constants are also provided that define the boundaries of valid TAI64
    representations.
    
    {b Warning:} This implementation obtains the current time of day using the
    POSIX [time()] function, which returns a value based on the UTC timescale
    (but with leap seconds "elided" in a way that makes conversions between
    POSIX time, Standard Time and TAI a perilous undertaking).  See the
    {!Cf_stdtime} module for details.
*)

(** {6 Types} *)

(** Abstract values of TAI64 type *)
type t

(** {6 Exceptions} *)

exception Range_error    (** Result not representable in TAI64 format. *)
exception Label_error    (** Input is not a valid TAI64 label *)

(** {6 Functions} *)

(** A total ordering function, defined so that the [Cf_tai64] module has the
    signature of the {!Cf_ordered.Total_T} module type.  [compare a b] compares
    two TAI64 values and returns either [-1], [0], or [1] depending on the
    relative total ordering of the values.
*)
val compare: t -> t -> int

(** Returns the current time in TAI64, obtained by reading the current time
    from the POSIX [time()] function, and adjusting for leap seconds.
    (Currently, the leap seconds table is hardcoded into the library, and the
    most recent leap second announcement was for Dec 31, 1998.)
*)
val now: unit -> t

(** The earliest TAI epoch representable in the TAI64 format.  The TAI64 label
    is [0000000000000000].
*)
val first: t

(** The latest TAI epoch representable in the TAI64 format.  The TAI64 label
    is [7fffffffffffffff].
*)
val last: t

(** Converts a TAI64 value to a value consistent with the result of calling the
    [Unix.gettimeofday] function.
*)
val to_unix_time: t -> float

(** Converts a value consistent with the result of calling the [Unix.time]
    function into a TAI64 value.
*)
val of_unix_time: float -> t

(** Returns a string of 8 characters containing the TAI64 label corresponding
    to the TAI64 value of its argument.
*)
val to_label: t -> string

(** Interprets the argument as a TAI64 label and returns the corresponding
    TAI64 value.  Raises [Label_error] if the label is not a valid TAI64 label.
*)
val of_label: string -> t

(** Add seconds to a TAI64 value.  Raises [Range_error] if the result is not
    a valid TAI64 value.
*)
val add: t -> int -> t

(** Add seconds to a TAI64 value.  Raises [Range_error] if the result is not
    a valid TAI64 value.
*)
val add_int32: t -> int32 -> t

(** Add seconds to a TAI64 value.  Raises [Range_error] if the result is not
    a valid TAI64 value.
*)
val add_int64: t -> int64 -> t

(** Subtract one TAI64 value from another.  [sub t0 t1] returns the number of
    seconds before [t0] that [t1] denotes.
*)
val sub: t -> t -> int64

(**/**)

(** The TAI64 value defining the beginning of the TAI64 epoch, i.e. 00:00:10,
    01 Jan 1970 UTC.  The TAI64 label is [4000000000000000].
*)
val epoch: t

(** The TAI epoch marking the start of the Modified Julian Day scale, i.e.
    00:00:00, 17 Nov 1858 TAI.  The TAI64 label is [3fffffff2efbbf8a].
*)
val mjd_epoch: t

(** Used by {!Cf_stdtime} for conversions from ISO 8601 timestamps to TAI64. *)
val leapsec_add: t -> bool -> t

(** Used by {!Cf_stdtime} for conversions from TAI64 to ISO 8601 timestamps. *)
val leapsec_sub: t -> bool * t

(*--- End of File [ cf_tai64.mli ] ---*)
