(*---------------------------------------------------------------------------*
  INTERFACE  cf_sock_common.mli

  Copyright (c) 2003-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

(** Object-oriented interface to extended network sockets interface.

    An alternative to the imperative functions interface described above is the
    object-oriented interface.  With the object-oriented interface, type
    constraints guard against more opportunities for runtime exceptions, but
    the API is substantially different from the Berkeley sockets API.
*)

(** The class type of network endpoints, i.e. objects containing socket
    descriptors and the specific methods they support.
*)
class type endpoint = object

    (** Returns the [Unix.file_descr] equivalent of the socket descriptor. *)
    method fd: Unix.file_descr

    (** Use [obj#send ?flags buf pos len] to send [len] octets from the string
        [buf] starting at position [pos], optionally with the flags indicated
        by [flags].  Returns the number of octets actually sent.  Raises
        [Unix.Error] if there is an error.  Raises [Invalid_argument] if [pos]
        and [len] do not correspond to a valid substring of [buf].
    *)
    method send: ?flags:Cf_socket.msg_flags -> string -> int -> int -> int
    
    (** Use [obj#recv ?flags buf pos len] to receive [len] octets to the string
        [buf] starting at position [pos], optionally with the flags indicated
        by [flags].  Returns the number of octets actually received.  Raises
        [Unix.Error] if there is an error.  Raises [Invalid_argument] if [pos]
        and [len] do not correspond to a valid substring of [buf].
    *)
    method recv: ?flags:Cf_socket.msg_flags -> string -> int -> int -> int
    
    (** Use [obj#shutdown cmd] to shutdown either sending or receiving (or
        both) on the socket.  Raises [Unix.Error] if there is an error.
    *)
    method shutdown: Unix.shutdown_command -> unit
    
    (** Use [obj#close] to close the socket. Raises [Unix.Error] if there is an
        error.
    *)
    method close: unit
end

(** The type of module created by the Create(P: P) functor. *)
module type T = sig

    (** The module input for the Create(P: Cf_socket.P) functor *)
    module P: Cf_socket.P
    
    (** The specialized socket type. *)
    type t = (P.AF.tag, P.ST.tag) Cf_socket.t
    
    (** The specialized address type. *)
    type address = P.AF.address
    
    (** Use [create ()] to create a new socket.  Raises [Unix.Error] if there
        is an error.
    *)
    val create: unit -> t
    
    (** Use [createpair ()] to create a pair of new sockets that are connected
        to one another.  Raises [Unix.Error] if there is an error.
    *)
    val createpair: unit -> t * t

    (** The base class for all sockets of this protocol.  Use [inherit basic
        ?sock ()] to derive a new class.  If [sock] is not provided, then a new
        socket is created, raising [Unix.Error] if an error is encountered.
    *)
    class basic:
        ?sock:t -> unit ->
        object
            val socket_: t      (** The socket *)
            
            (** Returns the socket. *)
            method socket: t
            
            (** Returns the [Unix.file_desc] corresponding to the socket. *)
            method fd: Unix.file_descr
            
            (** Use [obj#close] to close the socket.  Raises [Unix.Error] if
                there is an error.
            *)
            method close: unit
            
            (** Use [obj#dup] to duplicate the socket.  Raises [Unix.Error] if
                there is an error.
            *)
            method dup: t
            
            (** Use [obj#dup2 sock] to duplicate the socket, overwriting the
                socket [sock] in the process.  Raises [Unix.Error] if there is
                an error.
            *)
            method dup2: t -> unit
            
            (** Use [obj#getsockopt opt] to get the value of the socket option
                [opt].  Raises [Unix.Error] if there is an error.
            *)
            method getsockopt:
                'a. ('a, P.AF.tag, P.ST.tag) Cf_socket.sockopt -> 'a

            (** Use [obj#setsockopt opt v] to set the value of the socket
                option [opt].  Raises [Unix.Error] if there is an error.
            *)
            method setsockopt:
                'a. ('a, P.AF.tag, P.ST.tag) Cf_socket.sockopt -> 'a -> unit
        
            (** Use [self#getsockname] to get the locally bound endpoint
                address of the socket.  Raises [Unix.Error] if there is an
                error.
            *)
            method private getsockname: address
            
            (** Use [self#getsockname] to get the remotely bound endpoint
                address of the socket.  Raises [Unix.Error] if there is an
                error.
            *)
            method private getpeername: address
                        
            (** Use [obj#shutdown cmd] to shutdown either sending or receiving
                (or both) on the socket.  Raises [Unix.Error] if there is an
                error.
            *)
            method private shutdown: Unix.shutdown_command -> unit
            
            (** Use [self#bind sa] to bind the local endpoint address of the
                socket to the socket address [sa].  Raises [Unix.Error] if
                there is an error.
            *)
            method private bind: address -> unit
        end
end

(** The functor used to create the socket module. *)
module Create(P: Cf_socket.P): T with module P = P

(*--- End of File [ cf_sock_common.mli ] ---*)
