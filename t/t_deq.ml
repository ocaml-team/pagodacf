(*---------------------------------------------------------------------------*
  TEST  t_deq.ml

  Copyright (c) 2003, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

module type Deque_T = sig
    type 'a t
    
    val nil: 'a t
    
    module type Direction_T = sig
        val push: 'a -> 'a t -> 'a t
        val pop: 'a t -> ('a * 'a t) option
    end
    
    module A: Direction_T
    module B: Direction_T
    
    val catenate: 'a t -> 'a t -> 'a t
    val sprint: ('a -> string) -> 'a t -> string
end

module Test(D: Deque_T) = struct
    open D
    
    let loop ~x ~y ~z =
        let q = ref nil in
        for i = 1 to x do
            q := B.push i !q
        done;
        let k = ref 1 in
        for i = 1 to z do
            let q' = ref nil in
            for j = 1 to !k do
                incr k;
                if !k = y then k := 1;
                q' :=
                    match A.pop !q with
                    | Some (hd, tl) -> q := tl; B.push hd !q'
                    | None -> failwith "Deque error"
            done;
            q := catenate !q !q'
        done
    
    let test ~n ~x ~y ~z =
        for i = 1 to n do loop ~x ~y ~z done
end

module Kot_test = Test(Cf_kotdeque)
module Vw_test = Test(Cf_vwdeque)

let push = ref 1000
let pop = ref 100
let cat = ref 5000
let iterations = ref 1
let test = ref Vw_test.test

let usage = "usage: t.deq [options] { kot | vw }\n"

let speclist = [
    "-n", Arg.Int (fun n -> iterations := n), "iterations";
    "-x", Arg.Int (fun x -> push := x), "# push";
    "-y", Arg.Int (fun y -> push := y), "# pop per catenation";
    "-z", Arg.Int (fun z -> push := z), "# catenations"
]

let anonarg s =
    test :=
        if s = "kot" then
            Kot_test.test
        else if s = "vw" then
            Vw_test.test
        else
            failwith usage

let main () =
    Arg.parse speclist anonarg usage;
    !test ~n:!iterations ~x:!push ~y:!pop ~z:!cat;
    exit 0

;;

main ();;

(*--- End of File [ t_deq.ml ] ---*)
