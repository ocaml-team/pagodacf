(*---------------------------------------------------------------------------*
  INTERFACE  cf_sbheap.mli

  Copyright (c) 2002-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

(** Functional skew binomial heaps with O(1) merge. *)

(**
    This module implements a bootstrapped functional skew binomial heap, which
    have O(1) cost in space and time for most operations, including [merge].
    The underlying algorithm can be found in Chris Okasaki's
    {{:http://www.cs.cmu.edu/~rwh/theses/okasaki.pdf}Ph.D. thesis}.
*)

(** {6 Modules} *)

(** A functor that produces a module of type [Cf_heap] to represent heaps with
    the element type described by [E].
*)
module Heap(E: Cf_ordered.Total_T): Cf_heap.T with module Element = E

(** A functor that produces a module of type [Cf_pqueue] to represent priority
    queues with keys of the type described by [K].
*)
module PQueue(K: Cf_ordered.Total_T): Cf_pqueue.T with module Key = K

(*--- End of File [ cf_sbheap.mli ] ---*)
