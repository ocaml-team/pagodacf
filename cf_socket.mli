(*---------------------------------------------------------------------------*
  INTERFACE  cf_socket.mli

  Copyright (c) 2003-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

(** Extended network sockets interface. *)

(** {6 Overview}

    The Objective Caml [Unix] library contains a simplified interface for
    manipulating network sockets that is sufficient for most applications.
    This module and its cognates implement an alternative interface for using
    network sockets that offers the same extensibility as the Berkeley sockets
    interface does in the C language, while using the Objective Caml type
    system to enforce usage constraints.
*)

(** {6 Types} *)

(** The abstract type of socket type identifiers.  The type parameter is a
    shadow type attributed to the socket type identifier value.
*)
type 'st socktype

(** The abstract type of socket domain identifiers.  The type parameter is a
    shadow type attributed to the protocol/address family identifier value.
*)
type 'af domain

(** The abstract type of socket protocol identifiers. *)
type protocol

(** The abstract type of socket address structures.  Internally, these are
    represented as custom Objective Caml blocks of precisely the size of the
    underlying [struct sockaddr_xxx] structure.  The type parameter
    is a shadow type attributed to the address family identifier value.
*)
type 'af sockaddr

(** The abstract type of socket option identifiers.  The first type parameter
    is the type of values the socket option sets and gets.  The second type
    parameter is the shadow type used to constrain the use of the option to
    sockets of the appropriate protocol family.  The third type parameter is
    the shadow type used to constrain the use of the option to sockets of the
    appropriate socket type.
*)
type ('v,-'af,-'st) sockopt

(** The abstract type of socket descriptors.  The type parameters are the
    shadow types associated with the protocol/address family and the socket
    type of the socket, respectively.
*)
type ('af,'st) t

(** The record used to select the flags used with the [send] and [recv]
    functions (and their cognates).
*)
type msg_flags = {
    msg_oob: bool;          (** Message is out-of-band/expedited. *)
    msg_peek: bool;         (** Read message without dequeue. *)
    msg_dontroute: bool;    (** Use direct interface. *)
    msg_eor: bool;          (** End of record. *)
    msg_trunc: bool;        (** Message truncated in receive. *)
    msg_ctrunc: bool;       (** Control message truncated in receive. *)
    msg_waitall: bool;      (** Block until message completely received. *)
    msg_dontwait: bool;     (** Don't block. *)
    
    (**/**)
    
    (* __APPLE__ 
    msg_eof: bool;
    msg_flush: bool;
    msg_hold: bool;
    msg_send: bool;
    msg_havemore: bool;
    msg_rcvmore: bool;
    *)
}

(** {6 Modules and Module Types} *)

(** The module type used to define a socket address/protocol family. *)
module type AF = sig

    (** The shadow type *)
    type tag
    
    (** The concrete type of the socket address for the address family. *)
    type address
    
    (** The value of the socket domain identifier. *)
    val domain: tag domain

    (** Use [to_sockaddr a] to create an abstract socket address value
        corresponding to the address [a].
    *)
    val to_sockaddr: address -> tag sockaddr
    
    (** Use [of_sockaddr sa] to create an address corresponding to the abstract
        socket address value [sa].
    *)
    val of_sockaddr: tag sockaddr -> address
    
    (** The unspecified socket address, used for binding to arbitrary local
        endpoint addresses.
    *)
    val unspecified: tag sockaddr
end

(** The module type used to define a socket type. *)
module type ST = sig

    (** The shadow type. *)
    type tag
    
    (** The value of the socket type identifier. *)
    val socktype: tag socktype
end

(** The module type used to define a socket protocol. *)
module type P = sig

    (** The socket address/protocol family module. *)
    module AF: AF
    
    (** The socket type module. *)
    module ST: ST
    
    (** The abstract protocol identifier. *)
    val protocol: protocol
end

(** The module defining the [SOCK_STREAM] socket type. *)
module SOCK_STREAM: ST with type tag = [ `SOCK_STREAM ]
module SOCK_DGRAM: ST with type tag = [ `SOCK_DGRAM ]

(** {6 Constants} *)

(** The default value of the message flags structure, i.e. no flags set. *)
val msg_flags_none: msg_flags

(** {6 Functions}

    Most of the functions here are fairly straightforward wrappers around the
    Berkeley sockets API.  For a more convenient interface, consider using the
    object-oriented alternative, described in the next section.
*)

(** Use [create dom st p] to create a new socket descriptor with the socket
    domain [dom], the socket type [st] and the protocol identifier [p].  Raises
    [Unix.Error] if a system error occurs.
*)
val create: 'af domain -> 'st socktype -> protocol -> ('af,'st) t

(** Use [createpair dom st p] to create a pair of new socket descriptors
    that are already bound and connected to one another, using the socket
    domain [dom], the socket type [st] and the protocol identifier [p].  Raises
    [Unix.Error] if a system error occurs.
*)
val createpair:
    'af domain -> 'st socktype -> protocol -> (('af,'st) t as 'fd) * 'fd

(** Use [to_unix_file_descr sock] to obtain the file descriptor to use with
    functions in the [Unix] library that corresponds to the socket descriptor
    [sock].
*)
val to_unix_file_descr: ('af,'st) t -> Unix.file_descr

(** Use [domain_of_sockaddr sa] to obtain the socket domain identifier
    associated with a socket address of unknown address family.
*)
val domain_of_sockaddr: 'af sockaddr -> 'af domain

(** Use [dup sock] to create a duplicate of socket descriptor [sock].  Raises
    [Unix.Error] if there is an error.
*)
val dup: (('af,'st) t as 'fd) -> 'fd

(** Use [dup2 sock sock2] to create a duplicate of socket descriptor [sock] by
    overwriting the descriptor [sock2].  Raises [Unix.Error] if there is an
    error.
*)
val dup2: (('af,'st) t as 'fd) -> 'fd -> unit

(** Use [getsockname sock] to create a new socket address corresponding to the
    local bound endpoint of the socket [sock].  Raises [Unix.Error] if there is
    an error.
*)
val getsockname: ('af, 'st) t -> 'af sockaddr

(** Use [getpeername sock] to create a new socket address corresponding to the
    connected remote endpoint of the socket [sock].  Raises [Unix.Error] if
    there is an error.
*)
val getpeername: ('af, 'st) t -> 'af sockaddr

(** Use [bind sock sa] to bind the local endpoint address of the socket [sock]
    to the socket address [sa].  Raises [Unix.Error] if there is an error.
*)
val bind: ('af,'st) t -> 'af sockaddr -> unit

(** Use [connect sock sa] to connect the remote endpoint address of the socket
    [sock] to the socket address [sa].  Raises [Unix.Error] if there is an
    error.
*)
val connect: ('af,'st) t -> 'af sockaddr -> unit

(** Use [listen sock n] to set the socket [sock] into the mode of listening for
    connections with a backlog queue [n] spaces deep.  Raises [Unix.Error] if
    there is an error.
*)
val listen: ('af,([< `SOCK_STREAM | `SOCK_SEQPACKET ] as 'st)) t -> int -> unit

(** Use [accept sock] to accept a connected request on the listening socket
    [sock].  Returns a new socket descriptor and the socket address of the
    remote peer.  Raises [Unix.Error] if there is an error.
*)
val accept: ('af,([ `SOCK_STREAM ] as 'st)) t -> ('af,'st) t * 'af sockaddr

(** Use [shutdown sock cmd] to shutdown either sending or receiving (or both)
    on the socket [sock].  Raises [Unix.Error] if there is an error.
*)
val shutdown: ('af,'st) t -> Unix.shutdown_command -> unit

(** Use [close sock] to close a socket descriptor.  Raises [Unix.Error] if
    there is an error.
*)
val close: ('af,'st) t -> unit

(** Use [send sock buf pos len flags] to send [len] octets from the string
    [buf] starting at position [pos] on the socket [sock] with the flags
    indicated by [flags].  Returns the number of octets actually sent.  Raises
    [Unix.Error] if there is an error.  Raises [Invalid_argument] if [pos] and
    [len] do not correspond to a valid substring of [buf].
*)
val send: ('af, 'st) t -> string -> int -> int -> msg_flags -> int

(** Use [sendto sock buf pos len flags sa] to send [len] octets from the string
    [buf] starting at position [pos] on the socket [sock] with the flags
    indicated by [flags] to the socket address [sa].  Returns the number of
    octets actually sent.  Raises [Unix.Error] if there is an error.  Raises
    [Invalid_argument] if [pos] and [len] do not correspond to a valid
    substring of [buf].
*)
val sendto:
    ('af, ([ `SOCK_DGRAM ] as 'st)) t -> string -> int -> int -> msg_flags ->
    'af sockaddr -> int

(** Use [recv sock buf pos len flags] to receive [len] octets into the string
    [buf] starting at position [pos] on the socket [sock] with the flags
    indicated by [flags].  Returns the number of octets actually received.
    Raises [Unix.Error] if there is an error.  Raises [Invalid_argument] if
    [pos] and [len] do not correspond to a valid substring of [buf].
*)
val recv: ('af, 'st) t -> string -> int -> int -> msg_flags -> int

(** Use [recvfrom sock buf pos len flags] to receive [len] octets into the
    string [buf] starting at position [pos] on the socket [sock] with the flags
    indicated by [flags].  Returns the number of octets actually received and
    the socket address of the remote endpoint that sent them.  Raises
    [Unix.Error] if there is an error.  Raises [Invalid_argument] if [pos] and
    [len] do not correspond to a valid substring of [buf].
*)
val recvfrom:
    ('af, ([ `SOCK_DGRAM ] as 'st)) t -> string -> int -> int ->
    msg_flags -> int * 'af sockaddr

(** Use [getsockopt sock opt] to obtain the value associated with the socket
    option [opt] for the socket descriptor [sock].  Raises [Unix.Error] if
    there is an error.
*)
val getsockopt: ('af,'st) t -> ('v,'af,'st) sockopt -> 'v

(** Use [setsockopt sock opt v] to set the value associated with the socket
    option [opt] for the socket descriptor [sock] to the value [v].  Raises
    [Unix.Error] if there is an error.
*)
val setsockopt: ('af,'st) t -> ('v,'af,'st) sockopt -> 'v -> unit

(** {6 Socket Options}

    The following socket options are available on sockets of all socket types
    and address/protocol families.
*)

(** Enables recording of debugging information. *)
val so_debug: (bool,'af,'st) sockopt

(** Enables local address reuse. *)
val so_reuseaddr: (bool,'af,'st) sockopt

(** Enables duplicate address and port bindings. *)
val so_reuseport: (bool,'af,'st) sockopt

(** Enables connection keep-alives. *)
val so_keepalive: (bool,'af,'st) sockopt

(** Enables routing bypass for outgoing messages. *)
val so_dontroute: (bool,'af,'st) sockopt

(** Enables linger on close if data present. *)
val so_linger: (int option,'af,'st) sockopt

(** Enables permission to transmit broadcast messages. *)
val so_broadcast: (bool,'af,'st) sockopt

(** Enables in-band reception of out-of-band/expedited messages. *)
val so_oobinline: (bool,'af,'st) sockopt

(** Set buffer size for output. *)
val so_sndbuf: (int,'af,'st) sockopt

(** Set buffer size for input. *)
val so_rcvbuf: (int,'af,'st) sockopt

(** Set minimum octet count for output. *)
val so_sndlowat: (int,'af,'st) sockopt

(** Set minimum octet count for input. *)
val so_rcvlowat: (int,'af,'st) sockopt

(** Set timeout for output. *)
val so_sndtimeo: (float,'af,'st) sockopt

(** Set timeout for input. *)
val so_rcvtimeo: (float,'af,'st) sockopt

(** Get and clear the error on the socket (get only). *)
val so_error: (unit,'af,'st) sockopt

(** Do not generate SIGPIPE.  Instead raise [Unix.Error Unix.EPIPE]. *)
val so_nosigpipe: (bool,'af,'st) sockopt

(*--- End of File [ cf_socket.mli ] ---*)
