(*---------------------------------------------------------------------------*
  IMPLEMENTATION  cf_pqueue.ml

  Copyright (c) 2004-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

module type T = sig
    type +'a t

    module Key: sig type t end
    
    val nil: 'a t
    val empty: 'a t -> bool
    val size: 'a t -> int
    val head: 'a t -> (Key.t * 'a)
    val tail: 'a t -> 'a t
    val pop: 'a t -> ((Key.t * 'a) * 'a t) option
    val put: (Key.t * 'a) -> 'a t -> 'a t
    val merge: 'a t -> 'a t -> 'a t
    val iterate: ((Key.t * 'a) -> unit) -> 'a t -> unit
    val predicate: ((Key.t * 'a) -> bool) -> 'a t -> bool
    val fold: ('b -> (Key.t * 'a) -> 'b) -> 'b -> 'a t -> 'b
    val filter: ((Key.t * 'a) -> bool) -> 'a t -> 'a t
    val map: ((Key.t * 'a) -> 'b) -> 'a t -> 'b t
    val optmap: ((Key.t * 'a) -> 'b option) -> 'a t -> 'b t
    val partition: ((Key.t * 'a) -> bool) -> 'a t -> 'a t * 'a t
    val of_seq: (Key.t * 'a) Cf_seq.t -> 'a t
    val of_list: (Key.t * 'a) list -> 'a t
    val to_seq: 'a t -> (Key.t * 'a) Cf_seq.t
    val to_seq2: 'a t -> ((Key.t * 'a) * 'a t) Cf_seq.t
end

(*--- End of File [ cf_pqueue.ml ] ---*)
