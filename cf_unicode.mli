(*---------------------------------------------------------------------------*
  INTERFACE  cf_unicode.mli

  Copyright (c) 2003-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

(** Unicode transcodings. *)

(** This module contains a collection of utilities for transcoding streams of
    Unicode characters between various encoding formats, i.e. UTF8, UTF16 and
    UCS4.
*)

(** The module type defining the relationship between UCS4 and a given UTF
    encoding of Unicode characters.
*)
module type Encoding_T = sig

    (** A flow that transcodes an encoded character stream into a stream
        of 31-bit UCS4 integer codes.
    *)
    val to_ucs4: (char Cf_seq.t option, int) Cf_flow.t
    
    (** A flow that reads a stream of 31-bit UCS4 integer codes and writes
        them as a stream of encoded characters.
    *)
    val of_ucs4: (int, char) Cf_flow.t
end

(** The UTF8 encoding *)
module E_utf8: Encoding_T

(** The UTF16 encoding (big-endian) *)
module E_utf16be: Encoding_T

(** The UTF16 encoding (little-endian) *)
module E_utf16le: Encoding_T

(** A flow that transcodes any Unicode encoded character stream into a stream
    of 31-bit UCS4 integer codes.  Determines the encoding format by reading
    the barker code from the beginning of the input stream.
*)
val any_utf_to_ucs4: (char Cf_seq.t option, int) Cf_flow.t

(** Use [ucs4_to_utf16 endian] to compose a flow that reads a stream of 31-bit
    UCS4 integer codes and writes a stream comprised of the barker code
    followed by a stream of UTF16 characters in the 'endian' mode specified.
*)
val ucs4_to_utf16: [< `BE | `LE ] -> (int, char) Cf_flow.t

(** The module type that results from the application of the [C_create(E:
    Encoding_T)] functor below.  Modules of this type represent a transcoding
    from one Unicode encoding to another Unicode encoding.
*)
module type Transcoding_T = sig

    (** The encoding module used as the argument to the [C_create(E:
        Encoding_T)] functor.  The [to_ucs4] function defines the input
        encoding of the transcoder, and the [of_ucs4] function defines the
        output encoding.
    *)
    module E: Encoding_T
    
    (** A transcoder flow.  Use [transcoder] with the functions defined in the
        {!Cf_flow.Transcode} module to transcode between Unicode encodings in
        in stages.
    *)
    val transcoder: (char Cf_seq.t option, char) Cf_flow.t
    
    (** Use [transcode i] to convert a sequence of characters in the input
        encoding into a sequence of characters in the output encoding.
    *)
    val transcode: char Cf_seq.t -> char Cf_seq.t
    
    (** Use [atomic str] to convert a string in the input encoding into a
        string in the output encoding.
    *)
    val atomic: string -> string
end

(** The functor used to compose the transcoding modules defined below. *)
module C_create(E: Encoding_T): Transcoding_T with module E = E

(** Functions for transcoding UTF8 into UTF16 (big-endian) *)
module C_utf8_to_utf16be: Transcoding_T

(** Functions for transcoding UTF8 into UTF16 (little-endian) *)
module C_utf8_to_utf16le: Transcoding_T

(** Functions for transcoding UTF8 into UTF16 (big-endian, no barker code) *)
module C_utf8_to_utf16be_raw: Transcoding_T

(** Functions for transcoding UTF8 into UTF16 (little-endian, no barker
    code)
*)
module C_utf8_to_utf16le_raw: Transcoding_T

(** Functions for transcoding UTF16 (big-endian, no barker code) into UTF8. *)
module C_utf16be_to_utf8: Transcoding_T

(** Functions for transcoding UTF16 (little-endian, no barker code) into
    UTF8.
*)
module C_utf16le_to_utf8: Transcoding_T

(** Functions for transcoding any Unicode character stream into UTF8. *)
module C_any_utf_to_utf8: Transcoding_T

(** Functions for transcoding any Unicode character stream into UTF16
    (big-endian).
*)
module C_any_utf_to_utf16be: Transcoding_T

(** Functions for transcoding any Unicode character stream into UTF16
    (little-endian).
*)
module C_any_utf_to_utf16le: Transcoding_T

(** Functions for transcoding any Unicode character stream into UTF16
    (big-endian, no barker code).
*)
module C_any_utf_to_utf16be_raw: Transcoding_T

(** Functions for transcoding any Unicode character stream into UTF16
    (little-endian, no barker code).
*)
module C_any_utf_to_utf16le_raw: Transcoding_T

(*--- End of File [ cf_unicode.mli ] ---*)
