/*---------------------------------------------------------------------------*
  C MODULE  cf_ip_common_p.c

  Copyright (c) 2003-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*/

#include "cf_ip_common_p.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>

/*---
    type protocol_number_t =
        P_zero | P_icmp | P_ipv4 | P_ipv6 | P_tcp | P_udp
    
    external protocol_:
        protocol_number_t -> Cf_socket.protocol_t = "cf_ip_common_protocol"
  ---*/
CAMLprim value cf_ip_common_protocol(value numVal)
{
    CAMLparam1(numVal);
    
    static const int protocol[] = {
        /* P_zero */	0,
        /* P_icmp */	IPPROTO_ICMP,
        /* P_ipv4 */	IPPROTO_IP,
        /* P_ipv6 */	IPPROTO_IPV6,
        /* P_tcp */     IPPROTO_TCP,
        /* P_udp */     IPPROTO_UDP,
    };
    
    CAMLreturn(copy_nativeint(protocol[Int_val(numVal)]));
}

/*---
    type sockopt_index_t = TCP_NODELAY
  ---*/
static Cf_socket_sockopt_lift_t cf_ip_common_sockopt_lift_array[] = {
    { /* TCP_NODELAY */
        Val_unit,
        {
            IPPROTO_TCP, TCP_NODELAY,
            cf_socket_getsockopt_bool, cf_socket_setsockopt_bool
        }
    },
};

#define CF_IP_COMMON_SOCKOPT_LIFT_ARRAY_SIZE \
    (sizeof cf_ip_common_sockopt_lift_array / \
        sizeof cf_ip_common_sockopt_lift_array[0])

/*---
    external sockopt_lift:
        sockopt_index_t -> ('a,[<`AF_INET|`AF_INET6],'c) Cf_socket.sockopt_t =
        "cf_ip_common_sockopt_lift"
  ---*/
CAMLprim value cf_ip_common_sockopt_lift(value indexVal)
{
    CAMLparam1(indexVal);
    CAMLreturn(cf_ip_common_sockopt_lift_array[Int_val(indexVal)].ol_val);
}

/*---
  Initialization primitive
  ---*/
CAMLprim value cf_ip_common_init(value unit)
{
    int i;
            
    for (i = 0; i < CF_IP_COMMON_SOCKOPT_LIFT_ARRAY_SIZE; ++i) {        
        Cf_socket_sockopt_lift_t* liftPtr;
        
        liftPtr = &cf_ip_common_sockopt_lift_array[i];
        register_global_root(&liftPtr->ol_val);
        liftPtr->ol_val = cf_socket_option_alloc(&liftPtr->ol_option);
    }

    return Val_unit;
}

/*--- End of File [ cf_ip_common_p.c ] ---*/
