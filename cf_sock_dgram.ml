(*---------------------------------------------------------------------------*
  IMPLEMENTATION  cf_sock_dgram.ml

  Copyright (c) 2004-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

module type T = sig
    include Cf_sock_common.T with type P.ST.tag = [ `SOCK_DGRAM ]

    class endpoint:
        t ->
        object('self)
            inherit basic
            constraint 'self = #Cf_sock_common.endpoint
            
            method virtual getsockname: address

            method sendto:
                ?flags:Cf_socket.msg_flags -> string -> int -> int ->
                address -> int

            method recvfrom:
                ?flags:Cf_socket.msg_flags -> string -> int -> int ->
                int * address

            method connect: address -> unit

            method virtual getpeername: address

            method send:
                ?flags:Cf_socket.msg_flags -> string -> int -> int -> int
            method recv:
                ?flags:Cf_socket.msg_flags -> string -> int -> int -> int

            method virtual shutdown: Unix.shutdown_command -> unit
        end
end

module Create(P: Cf_socket.P with module ST = Cf_socket.SOCK_DGRAM) = struct
    include Cf_sock_common.Create(P)
    
    module S0 = Cf_socket
    
    class endpoint sock =
        object
            inherit basic ~sock ()
            
            method virtual getsockname: address

            method sendto ?(flags = S0.msg_flags_none) msg pos len addr =
                let addr = P.AF.to_sockaddr addr in
                S0.sendto socket_ msg pos len flags addr

            method recvfrom ?(flags = S0.msg_flags_none) msg pos len =
                let n, addr = S0.recvfrom socket_ msg pos len flags in
                n, P.AF.of_sockaddr addr

            method connect addr =
                let addr = P.AF.to_sockaddr addr in
                S0.connect socket_ addr

            method virtual getpeername: address
            
            method virtual shutdown: Unix.shutdown_command -> unit
            
            method send ?(flags = S0.msg_flags_none) msg pos len =
                S0.send socket_ msg pos len flags
            
            method recv ?(flags = S0.msg_flags_none) msg pos len =
                S0.recv socket_ msg pos len flags
            
        end
end

(*--- End of File [ cf_sock_dgram.ml ] ---*)
