(*---------------------------------------------------------------------------*
  INTERFACE  cf_sock_dgram.mli

  Copyright (c) 2004-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

(** Connection-oriented dgram transports with orderly release.

    This module extends the basic sockets interfaces defined in {!Cf_socket}
    and {!Cf_sock_common} with support for sockets of type [SOCK_DGRAM], i.e.
    connectionless transport (w/ optional multicast).  UDP endpoints are
    sockets of this type.
*)

(** The type of the module containing the extensions to the {!Cf_sock_common.T}
    module type used for handling sockets of the [SOCK_DGRAM] socket type.
*)
module type T = sig
    include Cf_sock_common.T with type P.ST.tag = [ `SOCK_DGRAM ]

    (** Use [new endpoint sock] to construct an object derived from
        {!Cf_sock_common.T.basic} that sports methods for sending data to one
        or more endpoints using the socket [sock].
    *)
    class endpoint:
        t ->
        object('self)
            inherit basic
            constraint 'self = #Cf_sock_common.endpoint
            
            (** Use [obj#getsockname] to obtain the actual local address
                associated with the socket.  Raises [Unix.Error] if there is an
                error.
            *)
            method virtual getsockname: address

            (** Use [self#sendto ?flags buf pos len addr] to send [len] octets
                from the string [buf] starting at position [pos] to the remote
                address [addr], and optionally with the flags indicated by
                [flags].  Returns the number of octets actually sent.  Raises
                [Unix.Error] if there is an error.  Raises [Invalid_argument]
                if [pos] and [len] do not correspond to a valid substring of
                [buf].
            *)
            method sendto:
                ?flags:Cf_socket.msg_flags -> string -> int -> int ->
                address -> int
            
            (** Use [recvfrom sock buf pos len flags] to receive [len] octets
                into the string [buf] starting at position [pos] on the socket
                [sock] with the flags indicated by [flags].  Returns the number
                of octets actually received and the socket address of the
                remote endpoint that sent them.  Raises [Unix.Error] if there
                is an error.  Raises [Invalid_argument] if [pos] and [len] do
                not correspond to a valid substring of [buf].
            *)
            method recvfrom:
                ?flags:Cf_socket.msg_flags -> string -> int -> int ->
                int * address

            (** Use [obj#connect] to associate the socket with the remote
                address [address].  Enables the use of the [getpeername],
                [send] and [recv] methods (may not be supported by all
                protocols).
            *)
            method connect: address -> unit

            (** Use [obj#getpeername] to obtain the actual remote address
                associated with the socket.  Raises [Unix.Error] if there is an
                error.  If there is no remote address associated with the
                socket, then a protocol-specific response is provided.  Most
                protocols will return an address indicated the remote address
                is unspecified.
            *)
            method virtual getpeername: address

            (** Use [self#send ?flags buf pos len] to send [len] octets from
                the string [buf] starting at position [pos] to the remote
                address previously associated with the socket using the
                [connect] method, and optionally with the flags indicated by
                [flags].  Returns the number of octets actually sent.  Raises
                [Unix.Error] if there is an error.  Raises [Invalid_argument]
                if [pos] and [len] do not correspond to a valid substring of
                [buf].
            *)
            method send:
                ?flags:Cf_socket.msg_flags -> string -> int -> int -> int

            (** Use [obj#recv ?flags buf pos len] to receive [len] octets to
                the string [buf] starting at position [pos] from the remote
                address previously associated with the socket using the
                [connect] method, and optionally with the flags indicated by
                [flags].  Returns the number of octets actually received.
                Raises [Unix.Error] if there is an error. Raises
                [Invalid_argument] if [pos] and [len] do not correspond to a
                valid substring of [buf].
            *)
            method recv:
                ?flags:Cf_socket.msg_flags -> string -> int -> int -> int
                    
            (** Use [obj#shutdown cmd] to shutdown either sending or receiving
                (or both) on the socket.  Raises [Unix.Error] if there is an
                error.
            *)
            method virtual shutdown: Unix.shutdown_command -> unit
        end
end

(** The functor used to create the socket module. *)
module Create(P: Cf_socket.P with module ST = Cf_socket.SOCK_DGRAM):
    T with module P = P

(*--- End of File [ cf_sock_dgram.mli ] ---*)
