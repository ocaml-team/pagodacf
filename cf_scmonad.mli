(*---------------------------------------------------------------------------*
  INTERFACE  cf_scmonad.mli

  Copyright (c) 2002-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

(** The state-continuation monad and its operators. *)

(** {6 Overview}
    
    The state-continuation monad is provided here purely for reference
    purposes.  It may be helpful as an example of how to lift the continuation
    monad into more complex monads.
*)

(** {6 Types} *)

(** The state-continuation monad. *)
type ('s, 'x, 'a) t = ('s -> 'x, 'a) Cf_cmonad.t

(** {6 Modules} *)

(** The continuation monad: a function for passing intermediate results from
    continuation context to continuation context with an encapsulated state
    value at each stage.
*)
module Op: sig
    (** Use [m >>= f] to produce a monad that applies [f] to the result of
        evaluating [m].
    *)
    val ( >>= ): ('s, 'x, 'a) t -> ('a -> ('s, 'x, 'b) t) -> ('s, 'x, 'b) t
end

(** {6 Operators} *)

(** A monad that returns [unit] and performs no operation. *)
val nil: ('s, 'x, unit) t

(** Use [return a] to produce a monad that returns [a] as an intermediate
    result from the current continuation.
*)
val return: 'a -> ('s, 'x, 'a) t

(** Use [init x] to produce a monad that discards the current intermediate
    result and returns [x] as the continuation context.
*)
val init: 'x -> ('s, 'x, 'a) t

(** Use [cont f] to produce a monad that passes the calling continuation to
    the function [f] and returns the unit value as an intermediate result.
*)
val cont: ('x -> 'x) -> ('s, 'x, unit) t

(** A monad that returns the encapsulate state as an intermediate result. *)
val load: ('s, 'x, 's) t

(** Use [store s] to produce a monad with [s] as the value of its encapsulated
    state.
*)
val store: 's -> ('s, 'x, unit) t

(** Use [modify f] to produce a monad that applies [f] to the encapsulated
    state to obtain a new state value, and which returns the unit value as its
    intermediate result.
*)
val modify: ('s -> 's) -> ('s, 'x, unit) t

(** Use [field f] to produce a monad that returns the result of applying [f] to
    the value of the encapsulated state.
*)
val field: ('s -> 'a) -> ('s, 'x, 'a) t

(** Use [down m s] to produce a stateless continuation monad from a
    state-continuation monad and an initial state.
*)
val down: ('s, 'x, unit) t -> 's -> ('x, 's) Cf_cmonad.t

(** Use [lift m] to lift a stateless continuation monad into a
    state-continuation monad.
*)
val lift: ('x, 'a) Cf_cmonad.t -> ('s, 'x, 'a) t

(*--- End of File [ cf_scmonad.mli ] ---*)
