(*---------------------------------------------------------------------------*
  IMPLEMENTATION  cf_ip_common.ml

  Copyright (c) 2003-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

external init_: unit -> unit = "cf_ip_common_init";;
init_ ();;

type protocol_number =
    P_zero | P_icmp | P_ipv4 | P_ipv6 | P_tcp | P_udp

external protocol_:
    protocol_number -> Cf_socket.protocol = "cf_ip_common_protocol"

let zero = protocol_ P_zero
let icmp = protocol_ P_icmp
let ipv4 = protocol_ P_ipv4
let ipv6 = protocol_ P_ipv6
let tcp = protocol_ P_tcp
let udp = protocol_ P_udp

type sockopt_index = TCP_NODELAY

external sockopt_lift:
    sockopt_index -> ('v,[<`AF_INET|`AF_INET6],'st) Cf_socket.sockopt =
    "cf_ip_common_sockopt_lift"

let tcp_nodelay = Obj.magic (sockopt_lift TCP_NODELAY)

(*--- End of File [ cf_ip_common.ml ] ---*)
