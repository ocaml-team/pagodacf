(*---------------------------------------------------------------------------*
  INTERFACE  cf_lexer.mli

  Copyright (c) 2002-2005, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

(** Lexical analysis with functional composition of regular grammars. *)

(** {6 Overview}

    This module implements functional parsers of the type defined in the
    {!Cf_parser.X} module using lazy deterministic finite automata.  Ordinary
    characters are the symbol type.
    
    {b Note}: functions for converting regular expression strings into lexer
    expressions are not provided.
*)

(** {6 Classes} *)

(** The class of cursors used by lazy DFA parser.  It inherits from the
    basic parser and defines a new method for handling errors.
*)

class cursor:
    int ->  (** The initial position, i.e. usually zero *)
    object('self)
        inherit [char] Cf_parser.cursor
        
        (** This method is invoked as [c#error n z] in a lexer when no rule
            matches the input stream [z] after [n] characters.  The purpose is
            to give a derived class an opportunity to raise an exception rather
            than allow the parser to return without a match.  In this base
            class, the method has no side effect.
        *)
        method error: int -> (char * 'self) Cf_seq.t -> unit
    end

(** {6 Types} *)

(** The type of lexer expressions. *)
type expr_t

(** The type of lexer rules. *)
type ('c, 'x) rule_t constraint 'c = #cursor

(** The type of lexical analyzers, which are defined as parsers that use input
    symbols of type [char].
*)
type ('c, 'x) t = ('c, char, 'x) Cf_parser.X.t constraint 'c = #cursor

(** {6 Functions} *)

(** Open this module to bring the operator functions into the current scope. *)
module Op: sig

    (** Use [!:c] to compose an expression that matches the character [c]. *)
    val ( !: ): char -> expr_t
    
    (** Use [!^f] to compose an expression that matches any character for which
        the satisfier function [f] returns [true].
    *)
    val ( !^ ): (char -> bool) -> expr_t
    
    (** Use [!~z] to compose an expression that matches the sequence of
        characters [z].
    *)
    val ( !~ ): char Cf_seq.t -> expr_t
    
    (** Use [!$lit] to compose an expression that matches the string literal
        [lit].
    *)
    val ( !$ ): string -> expr_t
    
    (** Alternating composition.  Use [a $| b] to compose an expression that
        matches either expression [a] or expression [b].
    *)
    val ( $| ): expr_t -> expr_t -> expr_t
    
    (** Serial composition.  Use [a $& b] to compose an expression that matches
        expression [a] followed by expression [b].
    *)
    val ( $& ): expr_t -> expr_t -> expr_t
    
    (** Star composition.  Use [!*a] to compose an expression that matches zero
        or any number of instances of [a].
    *)
    val ( !* ): expr_t -> expr_t
    
    (** Plus composition.  Use [!+a] to compose an expression that matches one
        or more instances of [a].
    *)
    val ( !+ ): expr_t -> expr_t
    
    (** Optional composition.  Use [!?a] to compose an expression that matches
        zero or one instance of [a].
    *)
    val ( !? ): expr_t -> expr_t
    
    (** Literal token rule.  Use [e $= obj] to compose a rule that outputs the
        literal object [obj] when the expression [e] is recognized.
    *)
    val ( $= ): expr_t -> 'x -> ('c, 'x) rule_t
    
    (** Character sequence token rule.  Use [e $> f] to compose a rule that
        applies the sequence of character recognized by the expression [e] to
        the tokenizer function [f] to produce its result.
    *)
    val ( $> ): expr_t -> (char Cf_seq.t -> 'x) -> ('c, 'x) rule_t
    
    (** String token rule.  Use [e $> f] to compose a rule that applies the
        string recognized by the expression [e] to the tokenizer function [f]
        to produce its result.
    *)
    val ( $^ ): expr_t -> (string -> 'x) -> ('c, 'x) rule_t
    
    (** Advanced token rule.  Use [e $@ f] to compose a rule that applies the
        length of the character sequence recognized by the expression [e] to
        the advanced tokenizer function [f] to obtain a parser that produces
        the output of the rule and makes any other manipulations necessary to
        continue parsing the input stream.  If the parser returned by [f] does
        not recognize the input, then no output is produced by the lexer.
    *)
    val ( $@ ): expr_t -> (int -> ('c, 'x) t) -> ('c, 'x) rule_t

    (** Rule aggregation.  Use this operator to combine a list of rules into a
        single rule.
    *)
    val ( !@ ): ('c, 'x) rule_t list -> ('c, 'x) rule_t
end

(** The lexer expression that recognizes the empty input sequence, i.e.
    epsilon.
*)
val nil: expr_t

(** Use [create ?xf r] to compose a lexical analyzer from the rule [r]. *)
val create: ('c, 'x) rule_t -> ('c, 'x) t

(** A record used by the [line_cursor] class defined below that indicates the
    character index, row and column in the input stream associated with a
    cursor position.
*)
type counter_t = {
    c_pos: int;     (** The character index (counts from zero). *)
    c_row: int;     (** The column number (counts from zero). *)
    c_col: int;     (** The row number (counts from zero). *)
}

(** The initial value of a cursor position counter. *)
val counter_zero: counter_t

(** A class derived from {!Cf_parser.cursor} that intercepts newline characters
    to track the row and column of a cursor position.  Use [new line_cursor ~c
    s] to construct an initial cursor position, optionally with the counter [c]
    (default: [counter_zero]), and a string [s] containing the character
    sequence that is recognized as a newline, e.g. "\013\010" indicates that
    newline is a CR LF sequence.
*)
class line_cursor:
    ?c:counter_t ->
    string ->
    object
        inherit cursor
        
        val row_: int           (** The current row number *)
        val col_: int           (** The current column number *)
        val nl0_: char list     (** The newline sequence as a [char list]. *)
        val nlz_: char list     (** The current tail of the newline. *)

        (** [self#next c] is called in the [advance] method to return a new
            values for the [row_], [col_] and [nlz_] members.
        *)
        method private next: char -> int * int * char list
        
        (** Returns a new counter object containing the row, column and index
            of the current cursor position.
        *)
        method counter: counter_t
        
        (** Returns the [row_] member. *)
        method row: int
        
        (** Returns the [col_] member. *)
        method col: int
    end

(** The exception raised by the [raise_exn] exception handler function below.
*)
exception Error of counter_t

(** An optional exception handler function for use in streams woven with
    cursors of the [#line_cursor] class type.  When the exception handler is
    called, the exception returned is constructed as [Error c#counter].
*)
val raise_exn: int -> (char * #line_cursor) Cf_seq.t -> exn

(*--- End of File [ cf_lexer.mli ] ---*)
