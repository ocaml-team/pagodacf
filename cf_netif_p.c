/*---------------------------------------------------------------------------*
  C MODULE  cf_netif_p.c

  Copyright (c) 2004-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*/

#include "cf_netif_p.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <net/if.h>
#include <errno.h>

/*---
  external nametoindex: string -> int = "cf_netif_nametoindex"
  ---*/
CAMLprim value cf_netif_nametoindex(value nameVal)
{
    CAMLparam1(nameVal);
    unsigned int n;
    
    n = if_nametoindex(String_val(nameVal));
    if (!n) raise_not_found();
    
    CAMLreturn(Val_int(n));
}

/*---
  external indextoname: string -> int = "cf_netif_indextoname"
  ---*/
CAMLprim value cf_netif_indextoname(value indexVal)
{
    CAMLparam1(indexVal);
    CAMLlocal1(nameVal);
    char buffer[IF_NAMESIZE];
    
    if (!if_indextoname(Int_val(indexVal), buffer)) raise_not_found();
    nameVal = copy_string(buffer);
    
    CAMLreturn(nameVal);
}

/*---
  external nameindex: unit -> (int * string) list = "cf_netif_nameindex"
  ---*/
CAMLprim value cf_netif_nameindex(value unit)
{
    CAMLparam0();
    CAMLlocal5(listVal, tailVal, nameVal, pairVal, cellVal);
    struct if_nameindex* mapPtr;
    struct if_nameindex* cellPtr;

    mapPtr = if_nameindex();
    if (!mapPtr) unix_error(errno, "if_nameindex", Nothing);

    listVal = Val_int(0);
    tailVal = Val_int(0);

    for (cellPtr = mapPtr; cellPtr->if_name; cellPtr++) {        
        nameVal = copy_string(cellPtr->if_name);
        
        pairVal = alloc_small(2, 0);
        Store_field(pairVal, 0, Val_int(cellPtr->if_index));
        Store_field(pairVal, 1, nameVal);
        
        cellVal = alloc_small(2, 0);
        Store_field(cellVal, 0, pairVal);
        Store_field(cellVal, 1, Val_int(0));
        
        if (!Is_block(listVal))
            listVal = cellVal;
        else
            modify(&Field(tailVal, 1), cellVal);
        
        tailVal = cellVal;
    }
    
    if_freenameindex(mapPtr);
    CAMLreturn(listVal);
}

/*--- End of File [ cf_netif_p.c ] ---*/
