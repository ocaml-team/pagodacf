(*---------------------------------------------------------------------------*
  INTERFACE  cf_regex.mli

  Copyright (c) 2005-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

(** Regular expression parsing, search and matching. *)

(** {6 Overview}

    This module implements regular expression parsing, search and matching in
    pure Objective Caml.  The grammar for regular expressions is a little
    unconventional.  Instead of using a backslash as the escape character, the
    backtick character is used instead.  This makes it easy to write regular
    expressions in string literals.
    
    Use any of the following constructions in regular expressions:
    - [`n     ] Matches LF ("newline") character.
    - [`t     ] Matches TAB character.
    - [`r     ] Matches RETURN character.
    - [`a     ] Matches an alphabetical character.
    - [`d     ] Matches a decimal digit character.
    - [`i     ] Matches an alphanumerical character.
    - [`s     ] Matches a TAB, LF, VT, FF, CR or SPACE (whitespace) character.
    - [`w     ] Matches a character other than a whitespace character.
    - [`xNN   ] Matches the character with hexadecimal code [NN].
    - [`DDD   ] Matches the character with decimal code [DDD], where DDD is a
        three digit number between [000] and [255].
    - [`c_    ] Matches the control character corresponding to the subsequent
        printable character, e.g. [`cA] is CONTROL-A, and [`c\[] is ESCAPE.
    - [.      ] Matches any character except newline.
    - [*      ] (postfix) Matches the preceding expression, zero, one or
        several times in sequence.
    - [+      ] (postfix) Matches the preceding expression, one or several
        times in sequence.
    - [?      ] (postfix) Matches the preceding expression once or not at all.
    - [[..]   ] Character set.  Ranges are denoted with ['-'], as in [[a-z]].
        An initial ['^'], as in [[^0-9]], complements the set.  Special
        characters in the character set syntax may be included in the set by
        escaping them with a backtick, e.g. [[`^```\]]] is a set containing
        three characters: the carat, the backtick and the right bracket
        characters.
    - [(..|..)] Alternatives.  Matches one of the expressions between the
        parentheses, which are separated by vertical bar characters.
    - [`_     ] Escaped special character.  The special characters are ['`'],
        ['.'], ['*'], ['+'], ['?'], ['('], ['|'], [')'], ['\['].
*)

(** {6 Modules} *)

(** The deterministic finite automata on octet character symbols. *)
module DFA: Cf_dfa.T with type S.t = char

(** {6 Exceptions} *)

(** An error parsing the specified string as a regular expression. *)
exception Error of string

(** {6 Types} *)

(** An abstract type representing a regular expression. *)
type t

(** {6 Functions} *)

(** A parser combinator on character streams that recognizes a regular
    expression and produces a DFA expression for it.
*)
val expr_parse: (char, DFA.x) Cf_parser.t

(** Use [expr_of_seq z] to evaluate the character sequence [z] as a regular
    expression and produce the corresponding DFA expression.  Raises [Error] if
    the sequence is not a valid expression.
*)
val expr_of_seq: char Cf_seq.t -> DFA.x

(** Use [expr_of_string s] to evaluate the string [s] as a regular expression
    and produce the corresponding DFA expression.  Raises [Error] if the string
    is not a valid expression.
*)
val expr_of_string: string -> DFA.x

(** A character flow that quotes all the special characters in the input so
    that the output may be used in a regular expression to match the input
    exactly.
*)
val quote: (char, char) Cf_flow.t

(** A character flow that unquotes all the quoted special characters in the
    input so that the output may by used in a regular expression to match the
    specified pattern.
*)
val unquote: (char, char) Cf_flow.t

(** Use [of_expression x] to produce a regular expression from the DFA
    expression [x].
*)
val of_expression: DFA.x -> t

(** Use [of_seq z] to ingest the whole character sequence [z], parse it and
    produce a regular expression.  Raises [Error s] if the sequence is
    not a valid regular expression, with [s] containing the string composed of
    the characters in the sequence.
*)
val of_seq: char Cf_seq.t -> t

(** Use [of_string s] to produce a regular expression from the string [s].
    Raises [Error s] if the string is not a valid regular expression.
*)
val of_string: string -> t

(** Use [test r s] to test whether the string [s] matches the regular
    expression [r].
*)
val test: t -> string -> bool

(** Use [search r z] to search the character sequence [z] for a pattern that
    matches the regular expression [r].  Returns [(pos, len)], where [pos] is
    the number of characters into the sequence where the matching sequence
    begins, and [len] is the number matching characters.
*)
val search: t -> char Cf_seq.t -> int * int

(** Use [separate r z] to map the character sequence [z] into the sequence of
    sequences found between matches for the regular expression [r].
*)
val separate: t -> char Cf_seq.t -> char Cf_seq.t Cf_seq.t

(** Use [split r s] to produce a list of strings by searching [s] left to right
    for blocks of characters between patterns that match the regular expression
    [r].
*)
val split: t -> string -> string list

(** Use [parse r] to produce a parser that matches the input stream to the
    regular expression [r] and returns the corresponding string value.
*)
val parse: t -> (char, string) Cf_parser.t

(** Use [parse r] to produce a parser that matches the input stream to the
    regular expression [r] and returns the corresponding string value.
*)
val parsex: t -> ('c, char, string) Cf_parser.X.t

(*--- End of File [ cf_regex.mli ] ---*)
