(*---------------------------------------------------------------------------*
  IMPLEMENTATION  cf_ip4_proto.ml

  Copyright (c) 2003-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

external init_: unit -> unit = "cf_ip4_proto_init";;
init_ ();;

external domain_: unit -> 'a Cf_socket.domain = "cf_ip4_proto_domain"

module AF = struct
    type tag = [ `AF_INET ]
    type address = Cf_ip4_addr.opaque Cf_ip4_addr.t * int
    
    let domain = domain_ ()

    external to_sockaddr:
        address -> tag Cf_socket.sockaddr = "cf_ip4_proto_to_sockaddr"
    external of_sockaddr:
        tag Cf_socket.sockaddr -> address = "cf_ip4_proto_of_sockaddr"

    let unspecified =
        to_sockaddr ((Cf_ip4_addr.any :> Cf_ip4_addr.opaque Cf_ip4_addr.t), 0)
end

module TCP = struct
    module AF = AF
    module ST = Cf_socket.SOCK_STREAM
    let protocol = Cf_ip_common.zero
end

module UDP = struct
    module AF = AF
    module ST = Cf_socket.SOCK_DGRAM
    let protocol = Cf_ip_common.zero
end

type mreq = {
    imr_multiaddr: Cf_ip4_addr.multicast Cf_ip4_addr.t;
    imr_interface: Cf_ip4_addr.unicast Cf_ip4_addr.t;
}

type sockopt_index =
    IP_TTL | IP_ADD_MEMBERSHIP | IP_DROP_MEMBERSHIP | IP_MULTICAST_IF |
    IP_MULTICAST_TTL | IP_MULTICAST_LOOP

external sockopt_lift:
    sockopt_index -> ('v,[`AF_INET],'st) Cf_socket.sockopt =
    "cf_ip4_proto_sockopt_lift"

let ip_ttl = Obj.magic (sockopt_lift IP_TTL)
let ip_add_membership = Obj.magic (sockopt_lift IP_ADD_MEMBERSHIP)
let ip_drop_membership = Obj.magic (sockopt_lift IP_DROP_MEMBERSHIP)
let ip_multicast_if = Obj.magic (sockopt_lift IP_MULTICAST_IF)
let ip_multicast_ttl = Obj.magic (sockopt_lift IP_MULTICAST_TTL)
let ip_multicast_loop = Obj.magic (sockopt_lift IP_MULTICAST_LOOP)

external siocgifaddr:
    ([ `AF_INET ], 'st) Cf_socket.t -> string ->
    [> Cf_ip4_addr.unicast ] Cf_ip4_addr.t = "cf_ip4_proto_siocgifaddr"

(*--- End of File [ cf_ip4_proto.ml ] ---*)
