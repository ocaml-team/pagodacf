(*---------------------------------------------------------------------------*
  IMPLEMENTATION  cf_socket.ml

  Copyright (c) 2003-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

external init_: unit -> unit = "cf_socket_init";;
init_ ();;

type 'st socktype
type 'af domain
type protocol
type 'af sockaddr
type ('af,'st) t

external create:
    'af domain -> 'st socktype -> protocol -> ('af,'st) t =
    "cf_socket_create"

external createpair:
    'af domain -> 'st socktype -> protocol ->
    (('af,'st) t as 'fd) * 'fd = "cf_socket_createpair"

external to_unix_file_descr:
    ('af,'st) t -> Unix.file_descr = "cf_socket_to_unix_file_descr"

external domain_of_sockaddr:
    'af sockaddr -> 'af domain = "cf_socket_domain_of_sockaddr"

external dup: (('af,'st) t as 'fd) -> 'fd = "cf_socket_dup"
external dup2: (('af,'st) t as 'fd) -> 'fd -> unit = "cf_socket_dup2"

external getsockname: ('af, 'st) t -> 'af sockaddr = "cf_socket_getsockname"
external getpeername: ('af, 'st) t -> 'af sockaddr = "cf_socket_getpeername"

external bind: ('af,'st) t -> 'af sockaddr -> unit = "cf_socket_bind"
external connect: ('af,'st) t -> 'af sockaddr -> unit = "cf_socket_connect"
external listen:
    ('af, ([< `SOCK_STREAM | `SOCK_SEQPACKET ] as 'st)) t -> int -> unit =
    "cf_socket_listen"
external accept:
    ('af, ([ `SOCK_STREAM ] as 'st)) t -> ('af, 'st) t * 'af sockaddr =
    "cf_socket_accept"

let shutdown sock cmd = Unix.shutdown (to_unix_file_descr sock) cmd
let close sock = Unix.close (to_unix_file_descr sock)

type msg_flags = {
    msg_oob: bool;
    msg_peek: bool;
    msg_dontroute: bool;
    msg_eor: bool;
    msg_trunc: bool;
    msg_ctrunc: bool;
    msg_waitall: bool;
    msg_dontwait: bool;
    
    (* __APPLE__ 
    msg_eof: bool;
    msg_flush: bool;
    msg_hold: bool;
    msg_send: bool;
    msg_havemore: bool;
    msg_rcvmore: bool;
    *)
}

let msg_flags_none = {
    msg_oob = false;
    msg_peek = false;
    msg_dontroute = false;
    msg_eor = false;
    msg_trunc = false;
    msg_ctrunc = false;
    msg_waitall = false;
    msg_dontwait = false;
    
    (* __APPLE__ 
    msg_eof = false;
    msg_flush = false;
    msg_hold = false;
    msg_send = false;
    msg_havemore = false;
    msg_rcvmore = false;
    *)
}

external send:
    ('af, 'st) t -> string -> int -> int -> msg_flags -> int =
    "cf_socket_send"
external sendto:
    ('af, ([ `SOCK_DGRAM ] as 'st)) t -> string -> int -> int -> msg_flags ->
    'af sockaddr -> int = "cf_socket_sendto_bytecode" "cf_socket_sendto_native"
external recv:
    ('af, 'st) t -> string -> int -> int -> msg_flags -> int = "cf_socket_recv"
external recvfrom:
    ('af, ([ `SOCK_DGRAM ] as 'st)) t -> string -> int -> int ->
    msg_flags -> int * 'af sockaddr = "cf_socket_recvfrom"

module type AF = sig
    type tag
    type address
    
    val domain: tag domain

    val to_sockaddr: address -> tag sockaddr
    val of_sockaddr: tag sockaddr -> address

    val unspecified: tag sockaddr
end

module type ST = sig
    type tag
    
    val socktype: tag socktype
end

module type P = sig
    module AF: AF
    module ST: ST
        
    val protocol: protocol
end

external sock_stream_init_: unit -> unit = "cf_sock_stream_init";;
sock_stream_init_ ();;

external sock_stream_socktype_lift_:
    unit -> 'a socktype = "cf_sock_stream_socktype_lift"

module SOCK_STREAM = struct
    type tag = [ `SOCK_STREAM ]
    let socktype = sock_stream_socktype_lift_ ()
end

external sock_dgram_init_: unit -> unit = "cf_sock_dgram_init";;
sock_dgram_init_ ();;

external sock_dgram_socktype_lift_:
    unit -> 'a socktype = "cf_sock_dgram_socktype_lift"

module SOCK_DGRAM = struct
    type tag = [ `SOCK_DGRAM ]
    let socktype = sock_dgram_socktype_lift_ ()
end

type ('v,-'af,-'st) sockopt

external getsockopt:
    ('af,'st) t -> ('v,'af,'st) sockopt -> 'v = "cf_socket_getsockopt"
external setsockopt:
    ('af,'st) t -> ('v,'af,'st) sockopt -> 'v -> unit =
    "cf_socket_setsockopt"

type sockopt_index =
    SO_DEBUG | SO_REUSEADDR | SO_REUSEPORT | SO_KEEPALIVE | SO_DONTROUTE |
    SO_LINGER | SO_BROADCAST | SO_OOBINLINE | SO_SNDBUF | SO_RCVBUF |
    SO_SNDLOWAT | SO_RCVLOWAT | SO_SNDTIMEO | SO_RCVTIMEO | SO_ERROR |
    SO_NOSIGPIPE

external sockopt_lift:
    sockopt_index -> ('a, 'b, 'c) sockopt = "cf_socket_sockopt_lift"

let so_debug = Obj.magic (sockopt_lift SO_DEBUG)
let so_reuseaddr = Obj.magic (sockopt_lift SO_REUSEADDR)
let so_reuseport = Obj.magic (sockopt_lift SO_REUSEPORT)
let so_keepalive = Obj.magic (sockopt_lift SO_KEEPALIVE)
let so_dontroute = Obj.magic (sockopt_lift SO_DONTROUTE)
let so_linger = Obj.magic (sockopt_lift SO_LINGER)
let so_broadcast = Obj.magic (sockopt_lift SO_BROADCAST)
let so_oobinline = Obj.magic (sockopt_lift SO_OOBINLINE)
let so_sndbuf = Obj.magic (sockopt_lift SO_SNDBUF)
let so_rcvbuf = Obj.magic (sockopt_lift SO_RCVBUF)
let so_sndlowat = Obj.magic (sockopt_lift SO_SNDLOWAT)
let so_rcvlowat = Obj.magic (sockopt_lift SO_RCVLOWAT)
let so_sndtimeo = Obj.magic (sockopt_lift SO_SNDTIMEO)
let so_rcvtimeo = Obj.magic (sockopt_lift SO_RCVTIMEO)
let so_error = Obj.magic (sockopt_lift SO_ERROR)
let so_nosigpipe = Obj.magic (sockopt_lift SO_NOSIGPIPE)

(*--- End of File [ cf_socket.ml ] ---*)
