(*---------------------------------------------------------------------------*
  INTERFACE  cf_sock_stream.mli

  Copyright (c) 2003-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

(** Connection-oriented stream transports with orderly release.

    This module extends the basic sockets interfaces defined in {!Cf_socket}
    and {!Cf_sock_common} with support for sockets of type [SOCK_STREAM], i.e.
    connection-oriented transports with orderly releases.  TCP endpoints are
    sockets of this type.
*)

(** The type of the module containing the extensions to the {!Cf_sock_common.T}
    module type used for handling sockets of the [SOCK_STREAM] socket type.
*)
module type T = sig
    include Cf_sock_common.T with type P.ST.tag = [ `SOCK_STREAM ]

    (** Use [new initiator ?sock ?src addr] to construct an object derived from
        {!Cf_sock_common.T.basic} that sports a method for connecting to a
        remote peer endpoint at the address [addr].  If the [?sock] argument is
        provided then the socket is treated as an unbound socket and used for
        the connection.  Otherwise, a new socket is created.  If the [?src]
        argument is provided, then the socket is bound to the specific address.
        Otherwise, the socket is bound to the unspecified address for the
        protocol/address family.  Raises [Unix.Error] if an error occurs.
    *)
    class initiator:
        ?sock:t -> ?src:address -> address ->
        object
            inherit basic
            
            (** Use [obj#getsockname] to obtain the actual local address
                associated with the socket.  Raises [Unix.Error] if there is an
                error.
            *)
            method virtual getsockname: address
            
            (** Use [obj#connect] to initiate the connection to the remote
                address provided to the constructor.
            *)
            method connect: unit
        end

    (** Use [new listener ?sock addr] to construct an object derived from
        {!Cf_sock_common.T.basic} that sports methods for passive listening
        for incoming connections at the local address [addr].  If the [?sock]
        argument is provided, then the socket is treated as a newly created
        socket and used for the listener.  Otherwise, a new socket is created.
        Raises [Unix.Error] if an error occurs.
    *)
    class listener:
        ?sock:t -> address ->
        object
            inherit basic
            
            (** Use [obj#getsockname] to obtain the actual local address
                associated with the socket.  Raises [Unix.Error] if there is an
                error.
            *)
            method virtual getsockname: address
            
            (** Use [obj#listen n] to place the socket into listening mode with
                a backlog queue depth of [n] pending connections.  Raises
                [Unix.Error] if there is an error.
            *)
            method listen: int -> unit
            
            (** Use [obj#accept] to accept a new connection from a remote peer.
                Returns a new socket and the address of its remote endpoint.
                Raises [Unix.Error] if an error occurs.
            *)
            method accept: t * address
        end

    (** Use [new endpoint sock] to construct an object derived from
        {!Cf_sock_common.T.basic} that sports methods for sending data to and
        receiving data from an already connected socket [sock].
    *)
    class endpoint:
        t ->
        object('self)
            inherit basic
            constraint 'self = #Cf_sock_common.endpoint
            
            (** Use [obj#getsockname] to obtain the actual local address
                associated with the socket.  Raises [Unix.Error] if there is an
                error.
            *)
            method virtual getsockname: address

            (** Use [obj#getpeername] to obtain the actual remote address
                associated with the socket.  Raises [Unix.Error] if there is an
                error.
            *)
            method virtual getpeername: address

            (** Use [self#send ?flags buf pos len] to send [len] octets from
                the string [buf] starting at position [pos], optionally with
                the flags indicated by [flags].  Returns the number of octets
                actually sent.  Raises [Unix.Error] if there is an error.
                Raises [Invalid_argument] if [pos] and [len] do not correspond
                to a valid substring of [buf].
            *)
            method send:
                ?flags:Cf_socket.msg_flags -> string -> int -> int -> int

            (** Use [obj#recv ?flags buf pos len] to receive [len] octets to
                the string [buf] starting at position [pos], optionally with
                the flags indicated by [flags].  Returns the number of octets
                actually received.  Raises [Unix.Error] if there is an error.
                Raises [Invalid_argument] if [pos] and [len] do not correspond
                to a valid substring of [buf].
            *)
            method recv:
                ?flags:Cf_socket.msg_flags -> string -> int -> int -> int
                    
            (** Use [obj#shutdown cmd] to shutdown either sending or receiving
                (or both) on the socket.  Raises [Unix.Error] if there is an
                error.
            *)
            method virtual shutdown: Unix.shutdown_command -> unit
        end
end

(** The functor used to create the socket module. *)
module Create(P: Cf_socket.P with module ST = Cf_socket.SOCK_STREAM):
    T with module P = P

(*--- End of File [ cf_sock_stream.mli ] ---*)
