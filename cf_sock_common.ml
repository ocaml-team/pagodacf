(*---------------------------------------------------------------------------*
  IMPLEMENTATION  cf_sock_common.ml

  Copyright (c) 2003-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

class type endpoint = object
    method fd: Unix.file_descr
    method send: ?flags:Cf_socket.msg_flags -> string -> int -> int -> int
    method recv: ?flags:Cf_socket.msg_flags -> string -> int -> int -> int
    method shutdown: Unix.shutdown_command -> unit
    method close: unit
end

module type T = sig
    module P: Cf_socket.P
    
    type t = (P.AF.tag, P.ST.tag) Cf_socket.t
    type address = P.AF.address
    
    val create: unit -> t
    val createpair: unit -> t * t

    class basic:
        ?sock:t -> unit ->
        object
            val socket_: t
            
            method socket: t
            method fd: Unix.file_descr
            method close: unit
            method dup: t
            method dup2: t -> unit
            
            method getsockopt: ('a, P.AF.tag, P.ST.tag) Cf_socket.sockopt -> 'a
            method setsockopt:
                ('a, P.AF.tag, P.ST.tag) Cf_socket.sockopt -> 'a -> unit
        
            method private getsockname: address
            method private getpeername: address
                        
            method private shutdown: Unix.shutdown_command -> unit
            
            method private bind: address -> unit
        end
end

module Create(P: Cf_socket.P) = struct
    module P = P
    
    type t = (P.AF.tag, P.ST.tag) Cf_socket.t
    type address = P.AF.address

    open Cf_socket
    
    let create () = create P.AF.domain P.ST.socktype P.protocol
    let createpair () = createpair P.AF.domain P.ST.socktype P.protocol

    class basic ?(sock = create ()) () =
        object
            val socket_ = sock
    
            method fd = to_unix_file_descr socket_
            method socket = socket_
            
            method dup = dup socket_
            method dup2 s = dup2 socket_ s
            
            method close = close socket_
            
            method getsockopt:
                'a. ('a,P.AF.tag, P.ST.tag) Cf_socket.sockopt -> 'a =
                fun opt -> getsockopt socket_ opt
            
            method setsockopt:
                'a. ('a,P.AF.tag, P.ST.tag) Cf_socket.sockopt -> 'a -> unit =
                fun opt x -> setsockopt socket_ opt x
        
            method private getsockname = P.AF.of_sockaddr (getsockname socket_)
            method private getpeername = P.AF.of_sockaddr (getpeername socket_)
                        
            method private shutdown cmd = shutdown socket_ cmd
            
            method private bind addr = bind socket_ (P.AF.to_sockaddr addr)
        end
end

(*--- End of File [ cf_sock_common.ml ] ---*)
