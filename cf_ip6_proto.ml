(*---------------------------------------------------------------------------*
  IMPLEMENTATION  cf_ip6_proto.ml

  Copyright (c) 2003-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

external init_: unit -> unit = "cf_ip6_proto_init";;
init_ ();;

external domain_: unit -> 'a Cf_socket.domain = "cf_ip6_proto_domain"

module AF = struct
    type tag = [ `AF_INET6 ]
    type address = Cf_ip6_addr.opaque Cf_ip6_addr.t * int * int32
    
    let domain = domain_ ()

    external to_sockaddr:
        address -> tag Cf_socket.sockaddr = "cf_ip6_proto_to_sockaddr"
    external of_sockaddr:
        tag Cf_socket.sockaddr -> address = "cf_ip6_proto_of_sockaddr"

    let unspecified =
        let any =
            (Cf_ip6_addr.unspecified :> Cf_ip6_addr.opaque Cf_ip6_addr.t)
        in
        to_sockaddr (any, 0, 0l)
end

module TCP = struct
    module AF = AF
    module ST = Cf_socket.SOCK_STREAM
    let protocol = Cf_ip_common.zero
end

module UDP = struct
    module AF = AF
    module ST = Cf_socket.SOCK_DGRAM
    let protocol = Cf_ip_common.zero
end

type mreq = {
    ipv6mr_multiaddr: Cf_ip4_addr.multicast Cf_ip4_addr.t;
    ipv6mr_interface: int;
}

type sockopt_index =
    IPV6_UNICAST_HOPS | IPV6_V6ONLY | IPV6_JOIN_GROUP | IPV6_LEAVE_GROUP |
    IPV6_MULTICAST_IF | IPV6_MULTICAST_HOPS | IPV6_MULTICAST_LOOP

external sockopt_lift:
    sockopt_index -> ('v,[`AF_INET6],'st) Cf_socket.sockopt =
    "cf_ip6_proto_sockopt_lift"

let ipv6_unicast_hops = Obj.magic (sockopt_lift IPV6_UNICAST_HOPS)
let ipv6_v6only = Obj.magic (sockopt_lift IPV6_V6ONLY)
let ipv6_join_group = Obj.magic (sockopt_lift IPV6_JOIN_GROUP)
let ipv6_leave_group = Obj.magic (sockopt_lift IPV6_LEAVE_GROUP)
let ipv6_multicast_if = Obj.magic (sockopt_lift IPV6_MULTICAST_IF)
let ipv6_multicast_hops = Obj.magic (sockopt_lift IPV6_MULTICAST_HOPS)
let ipv6_multicast_loop = Obj.magic (sockopt_lift IPV6_MULTICAST_LOOP)

(*--- End of File [ cf_ip6_proto.ml ] ---*)
