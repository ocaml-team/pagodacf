(*---------------------------------------------------------------------------*
  INTERFACE  cf_gregorian.mli

  Copyright (c) 2003-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

(** Conversions between the Gregorian calendar and Chronological Julian Day. *)

(** {6 Overview} *)

(** This module implements functions for converting dates in the Gregorian
    calendar to and from a Chronological Julian Day (CJD) number.  CJD numbers
    are signed integers, and day zero began at 00:00:00 TAI, 24 Nov -4713 CE.
    
    Gregorian dates between 12 Aug -2937947 CE and 27 Feb 2935093 inclusive
    are convertible to Chronological Julian Day numbers on platforms where the
    Ocaml integer is a 31-bit integer.
    
    Days of the month are represented by integers from 1 to 31.  Months of the
    year are represented by integers from 1 to 12.  Days of the week are
    represented by integers from 0 to 6, where 0 is Sunday and 6 is Saturday.
    Days of the year are numbered from 0 to 365, where only leap years contain
    a day numbered 365.
*)

(** {6 Functions} *)

(** [is_valid ~year ~month ~day] returns [true] if [year], [month] and [day]
    arguments specify a valid Gregorian date.
*)
val is_valid: year:int -> month:int -> day:int -> bool

(** [of_cjd ?wday ?yday cjd] returns a 3-tuple [(year, month, day)] with the
    year, month and day of the corresponding Gregorian date represented in
    integer form.
    
    If the [?wday] parameter is used, then the day of the week is computed and
    stored.  If the [?yday] parameter is used, then the day of the year is
    computed and stored.
    
    Raises [Invalid_argument] if [cjd] is in the very small range of values
    in the ancient past where the conversion algorithm would otherwise
    result in date in the extreme future.
*)
val of_cjd: ?wday:(int ref) -> ?yday:(int ref) -> int -> int * int * int

(** [to_cjd ~year ~month ~day] returns the Chronological Julian Day number of
    the specified Gregorian date.  Raise [Invalid_argument] if [year], [month]
    or [day] indicates an invalid Gregorian date, or the calculation would
    overflow the integer representation of CJD numbers.
*)
val to_cjd: year:int -> month:int -> day:int -> int

(** [to_cjd_unsafe ~year ~month ~day] is the same as [to_cjd ~year ~month
    ~day], except the parameters are not checked for boundaries and that the
    date is a valid Gregorian date.  Use this version if the date is already
    known to be valid, and in the range of representable CJD numbers.
*)
val to_cjd_unsafe: year:int -> month:int -> day:int -> int

(**/**)

(** The [of_mjd] function computes the Gregorian date from a Modified Julian
    Day number (which is a deprecated variant of the Julian Day reckoning that
    uses a different starting epoch).  It is used by the functions in the
    {!Cf_stdtime} module for conversions to TAI.
*)
val of_mjd: ?wday:(int ref) -> ?yday:(int ref) -> int -> int * int * int

(** The [to_mjd] function computes the Modified Julian Day number corresponding
    to a Gregorian calendar date.  It is used by the functions in the
    {!Cf_stdtime} module for conversions to TAI.
*)
val to_mjd: year:int -> month:int -> day:int -> int


(*--- End of File [ cf_gregorian.mli ] ---*)
